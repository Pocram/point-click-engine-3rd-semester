#ifndef _VectorMathH
#define _VectorMathH
#include <SFML/System/Vector2.hpp>

namespace VectorMath
{
	
	float Magnitude(sf::Vector2f v);
	sf::Vector2f SetMagnitude(sf::Vector2f v, float magnitude);

	sf::Vector2f Normalized(sf::Vector2f v);

}

#endif // _VectorMathH
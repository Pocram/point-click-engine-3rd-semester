#include "VectorMath.h"
#include "math.h"

float VectorMath::Magnitude(sf::Vector2f v)
{
	return sqrt(v.x * v.x + v.y * v.y);
}

sf::Vector2f VectorMath::SetMagnitude(sf::Vector2f v, float magnitude)
{
	return Normalized(v) * magnitude;
}

sf::Vector2f VectorMath::Normalized(sf::Vector2f v)
{
	float magnitude = Magnitude(v);
	v /= magnitude;
	return v;
}
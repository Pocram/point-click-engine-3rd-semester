#include "ObjectPool.h"
#include "boost/foreach.hpp"
#include <Engine/Scenes/GameScene.h>

extern GameScene CurrentScene;

// Constructors
GameObjectPool::GameObjectPool() 
{
	
}

GameObjectPool::GameObjectPool(int size, GameObject *prefab, bool instantiateInactive)
{
	if(prefab == NULL)
	{
		throw std::invalid_argument("Prefab GameObject was null!");
	}

	// Prepare pool
	m_pool.resize(size);

	for (int i = 0; i < size; i++) {
		// Instantiate every object
		GameObject newGo = *prefab;
		if(instantiateInactive)
		{
			newGo.SetActive(false);
		}

		CurrentScene.InstantiateGameObject(&newGo);
		m_pool[i] = &newGo;
	}
}

GameObjectPool::~GameObjectPool() {}


// Methods
void GameObjectPool::Resize(int newSize)
{
	m_pool.resize(newSize);
}
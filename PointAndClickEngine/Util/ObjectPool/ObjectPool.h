#ifndef __ObjectPoolH
#define __ObjectPoolH

#include <vector>
#include <Engine/GameObjects/GameObject.h>

class GameObjectPool {

public:
	// Constructors
	GameObjectPool();
	GameObjectPool(int size, GameObject *prefab, bool instantiateInactive = false);
	~GameObjectPool();

	// Methods
	void Resize(int newSize);

private:
	// Members
	std::vector<GameObject*> m_pool;

};

#endif // __ObjectPoolH
#include "Scene03_DimensionHole.h"
#include <Engine/GameObjects/Audio/AudioLoopObject.h>

#include <Game/Scene Specific/03_DimensionHole/DimensionHoleSceneBg.h>
#include <Game/Scene Specific/03_DimensionHole/DimensionEnterSoundObject.h>
#include <Game/Scene Specific/03_DimensionHole/DimensionHolePigObject.h>
#include <Game/Scene Specific/03_DimensionHole/DimensionHoleCrateObject.h>
#include <Game/Scene Specific/03_DimensionHole/DimensionHoleForgeObject.h>
#include <Game/Scene Specific/03_DimensionHole/DimensionHolePoorPigObject.h>
#include <Game/Scene Specific/03_DimensionHole/DimensionHoleEndGamePigObject.h>

Scene03_DimensionHole::Scene03_DimensionHole()
{
	m_name = "Dimension Hole";

	// BG
	// --------------------------------------------------------------
	DimensionHoleSceneBg *bg = new DimensionHoleSceneBg();
	bg->SetDrawOrder(0);
	InstantiateGameObject(bg);


	// Player
	// --------------------------------------------------------------
	PlayerObject *player = new PlayerObject();
	player->m_Name = "Player";
	player->SetPosition({ 140.0f, 270.0f }, true);
	player->SetDestination(player->GetTransform().getPosition());
	player->SetDrawOrder(100);
	InstantiateGameObject(player);
	SetPlayerObject(player);


	// Pig1
	// --------------------------------------------------------------
	DimensionHolePigObject *pig1 = new DimensionHolePigObject();
	pig1->m_Name = "Pig";
	pig1->SetPosition({ 90.0f, 280.0f }, true);
	pig1->SetDrawOrder(1);
	InstantiateGameObject(pig1);


	// Crate
	// --------------------------------------------------------------
	DimensionHoleCrateObject *crate = new DimensionHoleCrateObject();
	crate->m_Name = "Crate";
	crate->SetPosition({ 250.0f, 45.f }, true);
	crate->SetDrawOrder(2);
	InstantiateGameObject(crate);


	// Forge
	// --------------------------------------------------------------
	DimensionHoleForgeObject *forge = new DimensionHoleForgeObject();
	forge->m_Name = "Forge";
	forge->SetPosition({ 715.0f, 25.0f }, true);
	forge->SetDrawOrder(3);
	InstantiateGameObject(forge);


	// Poor pig
	// --------------------------------------------------------------
	DimensionHolePoorPigObject *poorPig = new DimensionHolePoorPigObject();
	poorPig->m_Name = "Poor Pig";
	poorPig->SetPosition({ 372.0f, 200.0f }, true);
	poorPig->SetDrawOrder(4);
	InstantiateGameObject(poorPig);


	// Guard pig
	// --------------------------------------------------------------
	DimensionHoleEndGamePigObject *guardPig = new DimensionHoleEndGamePigObject();
	guardPig->m_Name = "Guard Pig";
	guardPig->SetPosition({ 705.0f, 280.0f }, true);
	guardPig->SetDrawOrder(5);
	InstantiateGameObject(guardPig);


	// Ambience
	// --------------------------------------------------------------
	AudioLoopObject *ambienceAudio = new AudioLoopObject("Audio\\Ambience\\space.ogg");
	InstantiateGameObject(ambienceAudio);


	// Enter sound
	// --------------------------------------------------------------
	DimensionEnterSoundObject *enterSound = new DimensionEnterSoundObject();
	enterSound->m_Name = "Enter sound";
	InstantiateGameObject(enterSound);


	LoadSceneDataImage("Sprites/Scenes/3/sceneData");
}

Scene03_DimensionHole::~Scene03_DimensionHole() {}
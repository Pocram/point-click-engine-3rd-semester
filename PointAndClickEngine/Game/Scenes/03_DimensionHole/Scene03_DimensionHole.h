#ifndef __Scene03DimensionHoleH
#define __Scene03DimensionHoleH

#include <Engine/Scenes/GameScene.h>
#include <SFML/Audio.hpp>

class Scene03_DimensionHole : public GameScene {

public:
	Scene03_DimensionHole();
	~Scene03_DimensionHole();


private:
	sf::Sound m_doorOpenSound;
};

#endif // __Scene03DimensionHoleH
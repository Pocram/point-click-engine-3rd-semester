#include "Scene01_Forest.h"
#include <Game/Scene Specific/01_Forest/ForestSceneBg.h>
#include <Engine/GameObjects/Audio/AudioLoopObject.h>
#include <Game/Scene Specific/01_Forest/ForestStoneSmileyObject.h>
#include <Game/Scene Specific/01_Forest/ForestCagedPigObject.h>
#include <Game/Scene Specific/01_Forest/ForestCrateObject.h>
#include <Game/Scene Specific/01_Forest/ForestHammerObject.h>
#include <Game/Scene Specific/01_Forest/ForestEmptyBottleObject.h>
#include <Game/Scene Specific/01_Forest/ForestPigGuardObject.h>

Scene01_Forest::Scene01_Forest()
{
	m_name = "The Forest";

	// Background
	// --------------------------------------------------------------
	ForestSceneBg *backgroundImage = new ForestSceneBg();
	backgroundImage->SetDrawOrder(0);
	InstantiateGameObject(backgroundImage);

	// Player
	// --------------------------------------------------------------
	PlayerObject *player = new PlayerObject();
	player->m_Name = "Player";
	player->SetPosition({ 200.0f, 300.0f }, true);
	player->SetDestination(player->GetTransform().getPosition());
	player->SetDrawOrder(100);
	InstantiateGameObject(player);
	SetPlayerObject(player);


	// Stone Smiley
	ForestStoneSmileyObject *stoneSmiley = new ForestStoneSmileyObject();
	stoneSmiley->m_Name = "Stone Smiley";
	stoneSmiley->SetPosition({ 1484.0f, 315.0f }, true);
	InstantiateGameObject(stoneSmiley);


	// Caged oink
	ForestCagedPigObject *cagedPig = new ForestCagedPigObject();
	cagedPig->m_Name = "Caged Pig";
	cagedPig->SetPosition({240.f, 200.f}, true);
	cagedPig->SetDrawOrder(1);
	InstantiateGameObject(cagedPig);


	// Crate
	ForestCrateObject *crate = new ForestCrateObject();
	crate->m_Name = "Wooden Crate";
	crate->SetPosition({ 500.0f, 200.0f }, true);
	crate->SetDrawOrder(2);
	InstantiateGameObject(crate);


	// Hammer
	ForestHammerObject *hammer = new ForestHammerObject();
	hammer->m_Name = "Hammer";
	hammer->SetPosition({ 930.0f, 100.0f }, true);
	hammer->SetDrawOrder(3);
	InstantiateGameObject(hammer);


	// Hammer
	ForestEmptyBottleObject *bottle = new ForestEmptyBottleObject();
	bottle->m_Name = "Bottle";
	bottle->SetPosition({ 1300.0f, 200.0f }, true);
	bottle->SetDrawOrder(4);
	InstantiateGameObject(bottle);


	// Guard oink
	ForestPigGuardObject *guardPig = new ForestPigGuardObject();
	guardPig->m_Name = "Guard Pig";
	guardPig->SetPosition({ 1650.f, 200.f }, true);
	guardPig->SetDrawOrder(5);
	InstantiateGameObject(guardPig);

	// Ambience
	// --------------------------------------------------------------
	AudioLoopObject *ambienceAudio = new AudioLoopObject("Audio\\Ambience\\forest.wav");
	InstantiateGameObject(ambienceAudio);

	LoadSceneDataImage("Sprites/Scenes/1/sceneData");
}

Scene01_Forest::~Scene01_Forest() {}
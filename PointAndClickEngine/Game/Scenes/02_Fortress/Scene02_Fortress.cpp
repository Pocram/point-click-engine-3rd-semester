#include "Scene02_Fortress.h"
#include <Game/Scene Specific/02_Fortress/FortressSceneBg.h>
#include <Engine/GameObjects/Audio/AudioLoopObject.h>
#include <Game/Scene Specific/02_Fortress/FortressShadowsObject.h>
#include <Game/Scene Specific/02_Fortress/FortressGenericGuardPigObject.h>
#include <Game/Scene Specific/02_Fortress/FortressCakeObject.h>
#include <Game/Scene Specific/02_Fortress/FortressTweezersObject.h>
#include <Game/Scene Specific/02_Fortress/FortressCakePigObject.h>
#include <Game/Scene Specific/02_Fortress/FortressDoorObject.h>

Scene02_Fortress::Scene02_Fortress()
{
	m_name = "The Fortress";

	// BG
	// --------------------------------------------------------------
	FortressSceneBg *bg = new FortressSceneBg();
	bg->SetDrawOrder(0);
	InstantiateGameObject(bg);

	// Shadows
	// --------------------------------------------------------------
	FortressShadowsObject *shadows = new FortressShadowsObject();
	shadows->SetDrawOrder(999);
	InstantiateGameObject(shadows);


	// Player
	// --------------------------------------------------------------
	PlayerObject *player = new PlayerObject();
	player->m_Name = "Player";
	player->SetPosition({ 140.0f, 270.0f }, true);
	player->SetDestination(player->GetTransform().getPosition());
	player->SetDrawOrder(100);
	InstantiateGameObject(player);
	SetPlayerObject(player);

	
	// Guard 1
	// --------------------------------------------------------------
	FortressGenericGuardPigObject *guard1 = new FortressGenericGuardPigObject();
	guard1->m_Name = "Guard";
	guard1->SetDrawOrder(1);
	guard1->SetPosition({ 356.0f, 35.0f }, true);
	InstantiateGameObject(guard1);
	
	// Guard 2
	// --------------------------------------------------------------
	FortressGenericGuardPigObject *guard2 = new FortressGenericGuardPigObject();
	guard2->m_Name = "Guard";
	guard2->SetDrawOrder(2);
	guard2->SetPosition({ 577.0f, 320.0f }, true);
	InstantiateGameObject(guard2);
	
	// Guard 2
	// --------------------------------------------------------------
	FortressGenericGuardPigObject *guard3 = new FortressGenericGuardPigObject();
	guard3->m_Name = "Guard";
	guard3->SetDrawOrder(3);
	guard3->SetPosition({ 1010.0f, 35.0f }, true);
	InstantiateGameObject(guard3);


	// Caek
	// --------------------------------------------------------------
	FortressCakeObject *cake = new FortressCakeObject();
	cake->m_Name = "Cake";
	cake->SetDrawOrder(4);
	cake->SetPosition({ 850.0f, 35.0f }, true);
	InstantiateGameObject(cake);


	// Tweezers
	// --------------------------------------------------------------
	FortressTweezersObject *tweezers = new FortressTweezersObject();
	tweezers->m_Name = "Tweezers";
	tweezers->SetDrawOrder(5);
	tweezers->SetPosition({ 490.0f, 328.0f }, true);
	InstantiateGameObject(tweezers);


	// Hungry oink
	// --------------------------------------------------------------
	FortressCakePigObject *cakePig = new FortressCakePigObject();
	cakePig->m_Name = "Hungry Pig";
	cakePig->SetDrawOrder(6);
	cakePig->SetPosition({ 1510.0f, 80.0f }, true);
	InstantiateGameObject(cakePig);


	// Door
	// --------------------------------------------------------------
	FortressDoorObject *door = new FortressDoorObject();
	door->m_Name = "Door";
	door->SetDrawOrder(7);
	door->SetPosition({ 252.0f, 85.0f }, true);
	InstantiateGameObject(door);


	// Ambience
	// --------------------------------------------------------------
	AudioLoopObject *ambienceAudio = new AudioLoopObject("Audio\\Ambience\\crowd.ogg");
	InstantiateGameObject(ambienceAudio);

	LoadSceneDataImage("Sprites/Scenes/2/data");
}

Scene02_Fortress::~Scene02_Fortress() {}
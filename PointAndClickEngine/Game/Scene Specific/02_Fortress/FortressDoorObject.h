#ifndef __FortressDoorObjectH
#define __FortressDoorObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include <SFML/Audio.hpp>

class FortressDoorObject : public InteractableObject {

public:
	FortressDoorObject();
	~FortressDoorObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void CalculateBoundingBox() override;


private:
	sf::Sound m_unlockSound;

	bool m_isLocked;

};

#endif // __FortressDoorObjectH
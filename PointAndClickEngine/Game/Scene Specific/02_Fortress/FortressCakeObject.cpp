#include "FortressCakeObject.h"
#include <Engine/File Manager/TextureManager.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>
#include <Engine/File Manager/SoundManager.h>

FortressCakeObject::FortressCakeObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/2/glassCakeObject");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}

	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/glassShatter");
	if(sound != NULL)
	{
		m_shatterShound.setBuffer(*sound);
	}
}

FortressCakeObject::~FortressCakeObject() {}

void FortressCakeObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A cake, yum!", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void FortressCakeObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// No item?
	if (item == NULL) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("That would be too easy.", position, 2.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// 4 = hammer
	if(item->m_ID == 4)
	{
		SetActive(false);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);

		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("Yum, but it's full of shards now.", position, 3.0f);

		// 6 -> shard cake
		// YUM!
		Inventory::AddItemToInventory(6);
		m_shatterShound.play();

		return;
	} 
	else
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("No.", position, 3.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	}
}

void FortressCakeObject::PickUp()
{
}

void FortressCakeObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void FortressCakeObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
#include "FortressSceneBg.h"
#include "Engine/File Manager/TextureManager.h"

FortressSceneBg::FortressSceneBg()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/2/fortress");
	if(texture != NULL)
	{
		m_sprite.setTexture(*texture);
	}
}

FortressSceneBg::~FortressSceneBg() {}

void FortressSceneBg::Draw(sf::RenderWindow* window, sf::Transformable transform) 
{
	window->draw(m_sprite, transform.getTransform());
}
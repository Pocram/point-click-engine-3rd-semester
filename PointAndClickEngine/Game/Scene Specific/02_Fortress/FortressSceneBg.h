#ifndef __FortressSceneBgH
#define __FortressSceneBgH

#include "Engine/GameObjects/GameObject.h"

class FortressSceneBg :	public GameObject {

public:
	FortressSceneBg();
	~FortressSceneBg();

protected:
	virtual void Draw(sf::RenderWindow* window, sf::Transformable transform) override;

private:
	sf::Sprite m_sprite;

};

#endif // __FortressSceneBgH
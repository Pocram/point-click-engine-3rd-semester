#include "FortressDoorObject.h"
#include "Engine/File Manager/SoundManager.h"
#include "Engine/Game Logic/GameLogic.h"
#include "Engine/Scenes/Camera/Camera.h"
#include <Engine/GameLoop.h>
#include "Engine/Items/Inventory.h"
#include <Game/Scenes/03_DimensionHole/Scene03_DimensionHole.h>

FortressDoorObject::FortressDoorObject()
{
	sf::FloatRect bounds;
	bounds.width = 53.0f;
	bounds.height = 79.0f;

	bounds.top = GetTransform().getPosition().y;
	bounds.left = GetTransform().getPosition().x;

	m_boundingRect = bounds;
	m_isLocked = true;


	// Sound
	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/unlock");
	if(sound != NULL)
	{
		m_unlockSound.setBuffer(*sound);
	}
}

FortressDoorObject::~FortressDoorObject() {}

void FortressDoorObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A door. It's locked.", position, 1.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void FortressDoorObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if (item == NULL) {
		if(m_isLocked)
		{
			sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
			CurrentScene.ShowTextOnScreen("It's locked.", position, 2.0f);

			GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
			return;
		}
		else
		{
			LoadScene(Scene03_DimensionHole());
			GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		}
	}

	if(m_isLocked)
	{
		// 8 -> key
		if (item->m_ID == 8) {
			Inventory::RemoveItemFromInventory(8);
			m_unlockSound.play();

			sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
			CurrentScene.ShowTextOnScreen("It's unlocked now.", position, 2.0f);
			
			m_isLocked = false;

			GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);

			return;
		} else {
			sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
			CurrentScene.ShowTextOnScreen("No, because puzzle design.", position, 2.0f);

			GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
			return;
		}
	} 
	else
	{
		LoadScene(Scene03_DimensionHole());
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	}
}

void FortressDoorObject::PickUp()
{
}

void FortressDoorObject::CalculateBoundingBox()
{
	sf::FloatRect bounds;
	bounds.width = 53.0f;
	bounds.height = 79.0f;
	
	bounds.top = GetTransform().getPosition().y;
	bounds.left = GetTransform().getPosition().x;

	m_boundingRect = bounds;
}
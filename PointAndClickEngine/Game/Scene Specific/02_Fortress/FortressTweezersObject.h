#ifndef __FortressTweezersObjectH
#define __FortressTweezersObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include <SFML/Audio.hpp>

class FortressTweezersObject : public InteractableObject {

public:
	FortressTweezersObject();
	~FortressTweezersObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	sf::Sprite m_sprite;

	sf::Sound m_pickSound;

};

#endif // __FortressTweezersObjectH
#include "FortressGenericGuardPigObject.h"
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/File Manager/SoundManager.h>

FortressGenericGuardPigObject::FortressGenericGuardPigObject()
{
	m_animSprite = AnimatedSprite("Sprites/guardPig.png", { 64, 64 }, { 0, 0 }, { 3, 0 }, 0.25f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());


	// Load oink sound
	sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*testSoundBuffer);
}

FortressGenericGuardPigObject::~FortressGenericGuardPigObject() {}

void FortressGenericGuardPigObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A guard.", position, 1.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void FortressGenericGuardPigObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item != NULL)
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("Why would I do that?", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
	CurrentScene.ShowTextInWorld("Penmate. (oo)", position, 2.0f);
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	m_oinkSound.play();
}

void FortressGenericGuardPigObject::PickUp()
{
}

void FortressGenericGuardPigObject::OnUpdate(sf::Time elapsedTime)
{
	m_animSprite.Update(elapsedTime);
}

void FortressGenericGuardPigObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}

void FortressGenericGuardPigObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}
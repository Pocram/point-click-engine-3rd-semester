#include "FortressTweezersObject.h"
#include <Engine/File Manager/SoundManager.h>
#include <Engine/File Manager/TextureManager.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>

FortressTweezersObject::FortressTweezersObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/2/tweezersObject");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}

	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/cakeSquish");
	if (sound != NULL) {
		m_pickSound.setBuffer(*sound);
	}
}

FortressTweezersObject::~FortressTweezersObject() {}

void FortressTweezersObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("Well, tweezers. For tweezing.", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void FortressTweezersObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// No item?
	if (item == NULL) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("What would I tweeze, my hair?", position, 2.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// 6 = cake with shards
	if (item->m_ID == 6) {
		SetActive(false);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);

		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("All shards are gone!", position, 2.0f);

		// 7 -> cake
		// YUM!
		Inventory::RemoveItemFromInventory(6);
		Inventory::AddItemToInventory(7);
		m_pickSound.play();

		return;
	} 
	else 
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("No.", position, 1.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	}
}

void FortressTweezersObject::PickUp()
{
}

void FortressTweezersObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void FortressTweezersObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
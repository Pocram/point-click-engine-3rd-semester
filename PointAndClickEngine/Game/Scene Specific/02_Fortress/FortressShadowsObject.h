#ifndef __FortressShadowsH
#define __FortressShadowsH

#include <Engine/GameObjects/GameObject.h>

class FortressShadowsObject : public GameObject {

public:
	FortressShadowsObject();
	~FortressShadowsObject();


protected:
	virtual void Draw(sf::RenderWindow* window, sf::Transformable transform) override;


private:
	sf::Sprite m_sprite;

};

#endif // __FortressShadowsH
#include "FortressCakePigObject.h"
#include <Engine/File Manager/SoundManager.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>

FortressCakePigObject::FortressCakePigObject()
{
	m_animSprite = AnimatedSprite("Sprites/Scenes/1/cagedPig.png", { 64, 64 }, { 0, 1 }, { 3, 1 }, 0.25f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());


	// Load oink sound
	sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*testSoundBuffer);
}

FortressCakePigObject::~FortressCakePigObject() {}

void FortressCakePigObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A pig. It looks hungry!", position, 1.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void FortressCakePigObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if (item == NULL) 
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("I want cake. (oo)", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		m_oinkSound.play();
		return;
	}

	if(item->m_ID == 7)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Thanks! Take this. (oo)", position, 2.0f);

		Inventory::AddItemToInventory(8);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		m_oinkSound.play();

		Inventory::RemoveItemFromInventory(7);
		return;
	}
	else
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("What? (oo)", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		m_oinkSound.play();
	}
}

void FortressCakePigObject::PickUp()
{
}

void FortressCakePigObject::OnUpdate(sf::Time elapsedTime)
{
	m_animSprite.Update(elapsedTime);
}

void FortressCakePigObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}

void FortressCakePigObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}
#include "FortressShadowsObject.h"
#include <Engine/File Manager/TextureManager.h>

FortressShadowsObject::FortressShadowsObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/2/shadows");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
	}
}

FortressShadowsObject::~FortressShadowsObject() {}

void FortressShadowsObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}
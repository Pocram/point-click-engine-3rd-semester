#ifndef __DimensionHolePigObjec1tH
#define __DimensionHolePigObjec1tH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include <Engine/AnimatedSprite/AnimatedSprite.h>
#include <SFML/Audio.hpp>

class DimensionHolePigObject : public InteractableObject {

public:
	DimensionHolePigObject();
	~DimensionHolePigObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


	AnimatedSprite m_animSprite;
	sf::Sound m_oinkSound;

	bool m_gaveItem;

};

#endif // __DimensionHolePigObjec1tH
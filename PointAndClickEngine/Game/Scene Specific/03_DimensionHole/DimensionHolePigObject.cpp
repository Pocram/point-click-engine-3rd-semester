#include "DimensionHolePigObject.h"
#include <Engine/File Manager/SoundManager.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>

DimensionHolePigObject::DimensionHolePigObject()
{
	m_animSprite = AnimatedSprite("Sprites/Scenes/1/cagedPig.png", { 64, 64 }, { 0, 1 }, { 3, 1 }, 0.25f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());

	// Load oink sound
	sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*testSoundBuffer);

	m_gaveItem = false;
}

DimensionHolePigObject::~DimensionHolePigObject() {}

void DimensionHolePigObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A pig. It looks lazy!", position, 1.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void DimensionHolePigObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if (item == NULL) 
	{
		if(!m_gaveItem)
		{
			sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
			CurrentScene.ShowTextInWorld("I'm lazy. Take this. (oo)", position, 2.0f);

			m_gaveItem = true;
			Inventory::AddItemToInventory(9);

			GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
			m_oinkSound.play();
			return;
		}

		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Lazy? Uninspired! (oo)", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		m_oinkSound.play();
		return;
	}

	if(item != NULL)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("What? (oo)", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		m_oinkSound.play();
	}
}

void DimensionHolePigObject::PickUp()
{
}

void DimensionHolePigObject::OnUpdate(sf::Time elapsedTime)
{
	m_animSprite.Update(elapsedTime);
}

void DimensionHolePigObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}

void DimensionHolePigObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}
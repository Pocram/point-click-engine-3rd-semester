#include "DimensionHoleForgeObject.h"
#include "Engine/File Manager/SoundManager.h"
#include "Engine/File Manager/TextureManager.h"
#include "Engine/Game Logic/GameLogic.h"
#include "Engine/Scenes/Camera/Camera.h"
#include "Engine/Items/Inventory.h"

DimensionHoleForgeObject::DimensionHoleForgeObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/3/smelter");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}


	sf::SoundBuffer *unlockSound = SoundManager::GetSoundBuffer("Audio/SFX/unlock");
	if (unlockSound != NULL) {
		m_unlockSound.setBuffer(*unlockSound);
	}

	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/anvil");
	if (sound != NULL) {
		m_sound.setBuffer(*sound);
	}

	m_isLocked = true;
}

DimensionHoleForgeObject::~DimensionHoleForgeObject() {}

void DimensionHoleForgeObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A forge.", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void DimensionHoleForgeObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if (item == NULL) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm not going in there.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// Gold
	if (item->m_ID == 9 && !m_isLocked)
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("You forged a coin!", position, 2.0f);

		Inventory::RemoveItemFromInventory(9);
		Inventory::AddItemToInventory(11);
		m_sound.play();

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// Key
	if (item->m_ID == 10) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("You put the key into the forge.", position, 2.0f);

		Inventory::RemoveItemFromInventory(10);
		m_unlockSound.play();
		m_isLocked = false;

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}


	if(m_isLocked)
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("It's locked.", position, 2.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	}
	else
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("No.", position, 2.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	}
}

void DimensionHoleForgeObject::PickUp()
{

}

void DimensionHoleForgeObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void DimensionHoleForgeObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
#ifndef __DimensionHoleSceneBgH
#define __DimensionHoleSceneBgH

#include "Engine/GameObjects/GameObject.h"

class DimensionHoleSceneBg : public GameObject {

public:
	DimensionHoleSceneBg();
	~DimensionHoleSceneBg();

protected:
	virtual void Draw(sf::RenderWindow* window, sf::Transformable transform) override;

private:
	sf::Sprite m_sprite;

};

#endif // __DimensionHoleSceneBgH
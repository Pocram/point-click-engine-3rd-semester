#ifndef __DimensionHoleCrateObjectH
#define __DimensionHoleCrateObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include <SFML/Audio.hpp>

class DimensionHoleCrateObject : public InteractableObject {

public:
	DimensionHoleCrateObject();
	~DimensionHoleCrateObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	sf::Sprite m_sprite;

	sf::Sound m_sound;

};

#endif // __DimensionHoleCrateObjectH
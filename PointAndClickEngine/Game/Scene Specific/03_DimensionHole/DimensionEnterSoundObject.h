#ifndef __DimensionEnterSoundObjectH
#define __DimensionEnterSoundObjectH

#include "Engine/GameObjects/GameObject.h"
#include <SFML/Audio.hpp>

class DimensionEnterSoundObject : public GameObject {

public:
	DimensionEnterSoundObject();
	~DimensionEnterSoundObject();


private:
	sf::Sound m_sound;

};

#endif // __DimensionEnterSoundObjectH
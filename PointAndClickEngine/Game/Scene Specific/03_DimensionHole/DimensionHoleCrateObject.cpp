#include "DimensionHoleCrateObject.h"
#include "Engine/File Manager/SoundManager.h"
#include "Engine/File Manager/TextureManager.h"
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>

DimensionHoleCrateObject::DimensionHoleCrateObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/1/crate");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}

	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/woodBreak");
	if (sound != NULL) {
		m_sound.setBuffer(*sound);
	}
}

DimensionHoleCrateObject::~DimensionHoleCrateObject() {}

void DimensionHoleCrateObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A crate! Again!", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void DimensionHoleCrateObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item == NULL)
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("It's closed.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item->m_ID == 4)
	{
		Inventory::AddItemToInventory(10);

		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("There was yet another key inside!", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);

		m_sound.play();
		SetActive(false);
		return;
	}
}

void DimensionHoleCrateObject::PickUp()
{
}

void DimensionHoleCrateObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void DimensionHoleCrateObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
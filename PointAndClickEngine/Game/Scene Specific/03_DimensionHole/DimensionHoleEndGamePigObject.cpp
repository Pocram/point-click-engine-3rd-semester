#include "DimensionHoleEndGamePigObject.h"
#include "Engine/File Manager/SoundManager.h"
#include "Engine/Game Logic/GameLogic.h"
#include "Engine/Scenes/Camera/Camera.h"
#include "Engine/Items/Inventory.h"

DimensionHoleEndGamePigObject::DimensionHoleEndGamePigObject()
{
	m_animSprite = AnimatedSprite("Sprites/guardPig.png", { 64, 64 }, { 0, 0 }, { 3, 0 }, 0.25f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());

	// Load oink sound
	sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*testSoundBuffer);

	m_hasWon = false;
}

DimensionHoleEndGamePigObject::~DimensionHoleEndGamePigObject() {}

void DimensionHoleEndGamePigObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A pig. It looks important!", position, 1.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void DimensionHoleEndGamePigObject::UseWith(Item* item) 
{
	if (m_hasWon)
		return;

	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) 
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if (item == NULL || item->m_ID != 12) 
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Only winners allowed! (oo)", position, 2.0f);

		m_oinkSound.play();
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
	CurrentScene.ShowTextInWorld("Congratulations! You won! (oo)", position, 60.0f);

	position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("Yeah, I don't even know. It's not about the design anyway.", position, 60.0f);

	Inventory::RemoveItemFromInventory(12);

	m_hasWon = true;

	m_oinkSound.play();
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	return;
}

void DimensionHoleEndGamePigObject::PickUp()
{
}

void DimensionHoleEndGamePigObject::OnUpdate(sf::Time elapsedTime)
{
	m_animSprite.Update(elapsedTime);
}

void DimensionHoleEndGamePigObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}

void DimensionHoleEndGamePigObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}
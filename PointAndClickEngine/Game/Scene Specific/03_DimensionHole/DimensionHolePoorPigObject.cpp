#include "DimensionHolePoorPigObject.h"
#include "Engine/File Manager/SoundManager.h"
#include "Engine/Game Logic/GameLogic.h"
#include "Engine/Scenes/Camera/Camera.h"
#include "Engine/Items/Inventory.h"

DimensionHolePoorPigObject::DimensionHolePoorPigObject()
{
	m_animSprite = AnimatedSprite("Sprites/Scenes/3/cagedPig.png", { 64, 64 }, { 0, 1 }, { 3, 1 }, 0.25f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());

	// Load oink sound
	sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*testSoundBuffer);
}

DimensionHolePoorPigObject::~DimensionHolePoorPigObject() {}

void DimensionHolePoorPigObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A pig. It looks poor!", position, 1.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void DimensionHolePoorPigObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item == NULL)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Spare some change? (oo)", position, 2.0f);

		m_oinkSound.play();
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item->m_ID == 11)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Thank you! Take this! (oo)", position, 2.0f);

		Inventory::RemoveItemFromInventory(11);
		Inventory::AddItemToInventory(12);

		m_oinkSound.play();
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
	CurrentScene.ShowTextInWorld("What? (oo)", position, 2.0f);

	m_oinkSound.play();
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void DimensionHolePoorPigObject::PickUp()
{
}

void DimensionHolePoorPigObject::OnUpdate(sf::Time elapsedTime)
{
	m_animSprite.Update(elapsedTime);
}

void DimensionHolePoorPigObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}

void DimensionHolePoorPigObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}
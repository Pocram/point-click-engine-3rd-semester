#ifndef __DimensionHoleForgeObjectH
#define __DimensionHoleForgeObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include <SFML/Audio.hpp>

class DimensionHoleForgeObject : public InteractableObject {

public:
	DimensionHoleForgeObject();
	~DimensionHoleForgeObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	sf::Sprite m_sprite;

	sf::Sound m_sound;
	sf::Sound m_unlockSound;

	bool m_isLocked;
};

#endif // __DimensionHoleForgeObjectH
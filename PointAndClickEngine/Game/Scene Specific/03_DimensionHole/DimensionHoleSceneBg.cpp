#include "DimensionHoleSceneBg.h"
#include "Engine/File Manager/TextureManager.h"

DimensionHoleSceneBg::DimensionHoleSceneBg()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/3/bg");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
	}
}

DimensionHoleSceneBg::~DimensionHoleSceneBg()
{
}

void DimensionHoleSceneBg::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}
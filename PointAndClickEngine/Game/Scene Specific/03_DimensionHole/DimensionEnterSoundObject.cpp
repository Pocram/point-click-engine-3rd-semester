#include "DimensionEnterSoundObject.h"
#include <Engine/File Manager/SoundManager.h>

DimensionEnterSoundObject::DimensionEnterSoundObject()
{
	// Door opening sound because changing the scene while playing a sound stops it.
	// Also, playing it in the scene itself does not work either
	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/woodCreak");
	if (sound != NULL) {
		m_sound.setBuffer(*sound);
		m_sound.play();
	}
}

DimensionEnterSoundObject::~DimensionEnterSoundObject() {}
#ifndef __DimensionHolePoorPigObjectH
#define __DimensionHolePoorPigObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include <SFML/Audio.hpp>
#include "Engine/AnimatedSprite/AnimatedSprite.h"

class DimensionHolePoorPigObject : public InteractableObject {

public:
	DimensionHolePoorPigObject();
	~DimensionHolePoorPigObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


	AnimatedSprite m_animSprite;
	sf::Sound m_oinkSound;

};

#endif // __DimensionHolePoorPigObjectH
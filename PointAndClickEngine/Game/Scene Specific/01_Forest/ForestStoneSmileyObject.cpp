#include "ForestStoneSmileyObject.h"

#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Scenes/GameScene.h>
#include <Engine/UI/Buttons/ActionButtons/ActionUiButton.h>

extern GameScene CurrentScene;

ForestStoneSmileyObject::ForestStoneSmileyObject()
{
	CalculateBoundingBox();
}

ForestStoneSmileyObject::~ForestStoneSmileyObject() {}


void ForestStoneSmileyObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f});
	CurrentScene.ShowTextOnScreen("Stones :)", position, 2.0f);
	
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestStoneSmileyObject::CalculateBoundingBox()
{
	sf::FloatRect newBounds;
	newBounds.width = 65.0f;
	newBounds.height = 55.0f;
	newBounds.top = GetTransform().getPosition().y;
	newBounds.left = GetTransform().getPosition().x;

	m_boundingRect = newBounds;
}
#ifndef __ForestPigGuardObjectH
#define __ForestPigGuardObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"
#include "Engine/AnimatedSprite/AnimatedSprite.h"
#include <SFML/Audio.hpp>

class ForestPigGuardObject : public InteractableObject {

public:
	ForestPigGuardObject();
	~ForestPigGuardObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	AnimatedSprite m_animatedSprite;
	sf::Sound m_oinkSound;

};

#endif // __ForestPigGuardObjectH
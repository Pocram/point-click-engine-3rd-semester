#ifndef __ForestHammerObjectH
#define __ForestHammerObjectH

#include "Engine/GameObjects/Interactables/InteractableObject.h"

class ForestHammerObject : public InteractableObject {

public:
	ForestHammerObject();
	~ForestHammerObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	sf::Sprite m_sprite;

};

#endif // __ForestHammerObjectH
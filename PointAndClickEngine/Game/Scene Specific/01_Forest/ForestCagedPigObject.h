#ifndef __ForestCagedPigObjectH
#define __ForestCagedPigObjectH

#include <Engine/GameObjects/Interactables/InteractableObject.h>
#include <Engine/AnimatedSprite/AnimatedSprite.h>
#include <SFML/Audio.hpp>

class ForestCagedPigObject : public InteractableObject {

public:
	ForestCagedPigObject();
	~ForestCagedPigObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	AnimatedSprite m_animatedSprite;
	sf::Sound m_oinkSound;
	sf::Sound m_cageSound;

	bool m_isCaged;

	void DoCagedBehaviour(Item* item);
	void DoFreedBehaviour(Item* item);

};

#endif // __ForestCagedPigObjectH
#include "ForestPigGuardObject.h"
#include "Engine/File Manager/SoundManager.h"
#include "Engine/Game Logic/GameLogic.h"
#include "Engine/Scenes/Camera/Camera.h"
#include "Engine/Items/Inventory.h"

#include "Engine/GameLoop.h"
#include <Game/Scenes/02_Fortress/Scene02_Fortress.h>

ForestPigGuardObject::ForestPigGuardObject()
{
	// Load animated sprite
	m_animatedSprite = AnimatedSprite("Sprites/guardPig.png", { 64, 64 }, { 0, 0 }, { 3, 0 }, 0.125f);
	CalculateBoundingBoxFromSprite(m_animatedSprite.GetSprite());

	// Load oink sound
	sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*testSoundBuffer);
}

ForestPigGuardObject::~ForestPigGuardObject() {}


void ForestPigGuardObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("Looks like a guard. Can it even see with that helmet on?", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestPigGuardObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item == NULL || item->m_ID != 2)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Only pigs allowed! (oo)", position, 2.0f);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		m_oinkSound.play();
		return;
	}

	// Bottled snort
	Inventory::RemoveItemFromInventory(2);
	m_oinkSound.play();
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);

	LoadScene(Scene02_Fortress());
}

void ForestPigGuardObject::PickUp()
{
}

void ForestPigGuardObject::OnUpdate(sf::Time elapsedTime)
{
	m_animatedSprite.Update(elapsedTime);
}

void ForestPigGuardObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animatedSprite.GetSprite(), transform.getTransform());
}

void ForestPigGuardObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animatedSprite.GetSprite());
}
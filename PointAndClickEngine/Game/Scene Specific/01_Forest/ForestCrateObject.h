#ifndef __ForestCrateObjectH
#define __ForestCrateObjectH

#include <Engine/GameObjects/Interactables/InteractableObject.h>
#include <SFML/Audio.hpp>

class ForestCrateObject : public InteractableObject {

public:
	ForestCrateObject();
	~ForestCrateObject();

	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	sf::Sprite m_sprite;

	sf::Sound m_breakSound;

};

#endif // __ForestCrateObjectH
#include "ForestHammerObject.h"
#include <Engine/File Manager/TextureManager.h>
#include <Engine/Scenes/GameScene.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>

extern GameScene CurrentScene;

ForestHammerObject::ForestHammerObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/1/hammer");
	if(texture != NULL)
	{
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}
}

ForestHammerObject::~ForestHammerObject() {}


void ForestHammerObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A big hammer.", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestHammerObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	// No item? Pick it up
	if(item == NULL)
	{
		SetActive(false);
		Inventory::AddItemToInventory(4);
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}


	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("Why would I do that?", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestHammerObject::PickUp()
{
	Inventory::AddItemToInventory(4);

	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("Could be useful.", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestHammerObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void ForestHammerObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
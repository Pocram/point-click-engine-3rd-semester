#include "ForestSceneBg.h"

#include <SFML/Graphics.hpp>
#include <Engine/File Manager/TextureManager.h>

ForestSceneBg::ForestSceneBg()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/1/forestBg");
	if (texture == NULL)
		return;

	m_sprite.setTexture(*texture);
}

ForestSceneBg::~ForestSceneBg() {}

void ForestSceneBg::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}
#ifndef __ForestStoneSmileyObjectH
#define __ForestStoneSmileyObjectH

#include <Engine/GameObjects/Interactables/InteractableObject.h>

class ForestStoneSmileyObject :	public InteractableObject {

public:
	ForestStoneSmileyObject();
	~ForestStoneSmileyObject();

	virtual void LookAt() override;

protected:
	virtual void CalculateBoundingBox() override;

};

#endif // __ForestStoneSmileyObjectH
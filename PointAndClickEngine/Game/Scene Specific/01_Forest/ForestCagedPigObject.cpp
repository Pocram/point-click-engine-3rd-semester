#include "ForestCagedPigObject.h"

#include <Engine/Scenes/GameScene.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Game Logic/GameLogic.h>

#include <Engine/Items/Inventory.h>
#include <Engine/File Manager/SoundManager.h>

extern GameScene CurrentScene;

ForestCagedPigObject::ForestCagedPigObject()
{
	// Load animated sprite
	m_animatedSprite = AnimatedSprite("Sprites/Scenes/1/cagedPig.png", { 64, 64 }, { 0, 0 }, { 3, 0 }, 0.125f);
	CalculateBoundingBoxFromSprite(m_animatedSprite.GetSprite());

	// Load oink sound
	sf::SoundBuffer* oinkBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
	m_oinkSound.setBuffer(*oinkBuffer);

	sf::SoundBuffer* creakBuffer = SoundManager::GetSoundBuffer("Audio/SFX/metalCreak");
	m_cageSound.setBuffer(*creakBuffer);

	m_isCaged = true;
}

ForestCagedPigObject::~ForestCagedPigObject() {}


void ForestCagedPigObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A pig in a cage. (oo)", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestCagedPigObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(m_isCaged)
	{
		DoCagedBehaviour(item);
	}
	else
	{
		DoFreedBehaviour(item);
	}
}

void ForestCagedPigObject::PickUp()
{
}


void ForestCagedPigObject::OnUpdate(sf::Time elapsedTime)
{
	m_animatedSprite.Update(elapsedTime);
}

void ForestCagedPigObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animatedSprite.GetSprite(), transform.getTransform());
}

void ForestCagedPigObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animatedSprite.GetSprite());
}

void ForestCagedPigObject::DoCagedBehaviour(Item* item)
{
	// Without an item, just talk.
	if (item == NULL) {
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Let me out! (oo)", position, 2.0f);
		m_oinkSound.play();
	} else if (item->m_ID == 3) {
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Thank you! (oo)", position, 2.0f);
		m_oinkSound.play();

		m_isCaged = false;
		m_cageSound.play();

		m_animatedSprite.SetAnimationRange({ 0, 1 }, { 3, 1 });

		Inventory::RemoveItemFromInventory(3);
	} else {
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("What are you trying to do? (oo)", position, 2.0f);
		m_oinkSound.play();
	}

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestCagedPigObject::DoFreedBehaviour(Item* item)
{
	if(item == NULL)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("I want to give you something. Bring me a container! (oo)", position, 5.0f);
		m_oinkSound.play();

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item->m_ID == 5)
	{
		sf::Vector2f position = GetTransform().getPosition() + sf::Vector2f(32.0f, 10.f);
		CurrentScene.ShowTextInWorld("Here, take this! (oo)", position, 2.0f);
		m_oinkSound.play();

		position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("The pig snorts into your bottle.", position, 5.0f);

		Inventory::RemoveItemFromInventory(5);
		Inventory::AddItemToInventory(2);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}
}
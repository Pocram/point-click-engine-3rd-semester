#include "ForestEmptyBottleObject.h"
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Items/Inventory.h>
#include <Engine/File Manager/TextureManager.h>

ForestEmptyBottleObject::ForestEmptyBottleObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/1/bottleEmpty");
	if (texture != NULL) {
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}
}

ForestEmptyBottleObject::~ForestEmptyBottleObject() {}


void ForestEmptyBottleObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("A bottle!", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestEmptyBottleObject::UseWith(Item* item)
{	
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if (item != NULL) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("But why?", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	SetActive(false);
	Inventory::AddItemToInventory(5);
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestEmptyBottleObject::PickUp()
{

}


void ForestEmptyBottleObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void ForestEmptyBottleObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
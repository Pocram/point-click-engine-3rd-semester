#include "ForestCrateObject.h"
#include <Engine/File Manager/TextureManager.h>
#include <Engine/Scenes/GameScene.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Scenes/Camera/Camera.h>

#include <Engine/Items/Inventory.h>
#include <Engine/File Manager/SoundManager.h>

extern GameScene CurrentScene;

ForestCrateObject::ForestCrateObject()
{
	sf::Texture *texture = TextureManager::GetTexture("Sprites/Scenes/1/crate");
	if(texture != NULL)
	{
		m_sprite.setTexture(*texture);
		CalculateBoundingBoxFromSprite(m_sprite);
	}

	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/woodBreak");
	if(sound != NULL)
	{
		m_breakSound.setBuffer(*sound);
	}
}

ForestCrateObject::~ForestCrateObject() {}

void ForestCrateObject::LookAt()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("Maybe I can open it with something?", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestCrateObject::UseWith(Item* item)
{
	// If the player is too far away do nothing.
	if (GetDistanceFromPlayer() > 200.0f) {
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I'm too far away.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item == NULL)
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("I can't use this.", position, 2.0f);

		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	if(item->m_ID == 4)
	{
		Inventory::AddItemToInventory(3);

		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("There was a key inside!", position, 2.0f);

		m_breakSound.play();
		
		// Disable the object
		SetActive(false);
	}
	else
	{
		sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
		CurrentScene.ShowTextOnScreen("That won't do.", position, 2.0f);
	}

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestCrateObject::PickUp()
{
	sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
	CurrentScene.ShowTextOnScreen("I can't pick this up.", position, 2.0f);

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void ForestCrateObject::OnUpdate(sf::Time elapsedTime)
{
}

void ForestCrateObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_sprite, transform.getTransform());
}

void ForestCrateObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_sprite);
}
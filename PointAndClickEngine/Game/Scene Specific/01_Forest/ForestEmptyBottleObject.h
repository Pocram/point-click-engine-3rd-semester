#ifndef __ForestEmptyBottleH
#define __ForestEmptyBottleH

#include <Engine/GameObjects/Interactables/InteractableObject.h>

class ForestEmptyBottleObject :	public InteractableObject {

public:
	ForestEmptyBottleObject();
	~ForestEmptyBottleObject();

	// Interactions
	virtual void LookAt() override;
	virtual void UseWith(Item* item) override;
	virtual void PickUp() override;


protected:
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	sf::Sprite m_sprite;

};

#endif // __ForestEmptyBottleH
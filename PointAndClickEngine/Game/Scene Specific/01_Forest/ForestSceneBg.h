#ifndef __ForestSceneBgH
#define __ForestSceneBgH

#include <Engine/GameObjects/GameObject.h>

class ForestSceneBg : public GameObject {

public:
	ForestSceneBg();
	~ForestSceneBg();


protected:
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;


private:
	sf::Sprite m_sprite;
};

#endif // __ForestSceneBgH
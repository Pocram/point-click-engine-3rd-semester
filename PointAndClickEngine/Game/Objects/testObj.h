#pragma once
#include <SFML/Audio.hpp>

#include <Engine/File Manager/SoundManager.h>

#include <Engine/GameObjects/GameObject.h>
#include <Engine/AnimatedSprite/AnimatedSprite.h>
#include <Engine/GameObjects/Interactables/InteractableObject.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/GameLoop.h>

class testObj :	public InteractableObject {

public:
	// Constructors
	testObj();
	~testObj();

	// Methods
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;

	virtual void LookAt() override;

	virtual void UseWith(Item* item) override;


	// Members


private:
	// Members
	sf::Texture m_texture;
	sf::Sprite m_sprite;

	AnimatedSprite m_animSprite;

	sf::Sound m_oinkSound;

};
#include "testObj.h"
#include <Engine/File Manager/FontManager.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/UI/GameInterface.h>

extern GameScene CurrentScene;

testObj::testObj()
{
	m_animSprite = AnimatedSprite("Sprites/spriteSheet.png", { 100, 100 }, { 0, 0 }, { 5, 0 }, .25f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}

testObj::~testObj() {}


void testObj::OnUpdate(sf::Time elapsedTime)
{
	m_animSprite.Update(elapsedTime);

	Translate(sf::Vector2f(50.0f, -5.0f) * elapsedTime.asSeconds());

	GameScene* currentScene = GameLogic::GetCurrentScene();
	if(currentScene != NULL)
	{
		bool isInChannel = currentScene->AreBoundsInRedChannel(GetBoundingBox());
		if(isInChannel)
		{
			m_animSprite.SetColor(sf::Color::White);
		}
		else
		{
			m_animSprite.SetColor(sf::Color::Red);
		}
	}
}

void testObj::Draw(sf::RenderWindow *window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}


void testObj::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}

void testObj::LookAt()
{
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	CurrentScene.ShowTextOnScreen("Oh god, you clicked me!", CurrentScene.GetCamera().ScreenToWindowPoint(sf::Vector2f(0.5f, 0.5f), true, true), 2.0f);
}

void testObj::UseWith(Item* item)
{
	if(item != NULL && item->m_ID == 2)
	{
		sf::SoundBuffer* testSoundBuffer = SoundManager::GetSoundBuffer("Audio/SFX/oink");
		m_oinkSound.setBuffer(*testSoundBuffer);
		m_oinkSound.play();
	}

	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	Translate(sf::Vector2f(20.0f, -40.0f), true);
}
#include <SFML/Graphics.hpp>
#include "windows.h"

#include <Engine/GameLoop.h>

#include <Engine/File Manager/FileManager.h>
#include <Engine/File Manager/FontManager.h>
#include <Engine/File Manager/SoundManager.h>
#include <Engine/File Manager/TextureManager.h>
#include "Engine/File Manager/ItemManager.h"


int main(int argc, char *argv[]) {
	// I'm writing my engine for Windows only, sorry!
#ifndef WIN32
	return 0;
#endif

	// Add a console window so we can log properly.
	// No, this method is not archaic, Markus.
	AllocConsole();
	SetConsoleTitle(TEXT("Console log"));

	// Tell the file manager where the executable is located.
	// This is used to find the asset folder later on.
	FileManager::GetApplicationDirectoryFromArgv(argv);

	// Load assets
	printf("Loading Assets...\n");
	sf::Clock assetLoadingClock;
	assetLoadingClock.restart();

	TextureManager::InitTextureManager();
	SoundManager::InitSoundManager();
	FontManager::InitFontManager();
	ItemManager::InitItemManager();

	printf("Finished loading assets after %f seconds!\n", assetLoadingClock.getElapsedTime().asSeconds());

	// Set up the render window
	int height = 500;
	float aspect = 16.f / 9.f;
	// The window has a title bar and can be closed, but not resized.
	sf::RenderWindow window(sf::VideoMode(static_cast<int>(height * aspect), height), "Pointless Click Engine", sf::Style::Titlebar | sf::Style::Close);
	
	// Load the first scene and start the game loop
	StartGameLoop(&window);

	// Clean up after exiting the game loop
	FreeConsole();
	window.close();
	return 0;
}
#ifndef __AnimatedSpriteH
#define __AnimatedSpriteH

#include "SFML/Graphics.hpp"

class AnimatedSprite {

public:
	// Constructors
	AnimatedSprite();
	AnimatedSprite(std::string pathToSpriteSheet, sf::Vector2i spriteDimensions, sf::Vector2i startPositionInSheet, sf::Vector2i endPositionInSheet, float timeBetweenFrames);
	~AnimatedSprite();


	// Methods
	void Update(sf::Time elapsedTime);
	void Draw(sf::RenderWindow *window);

	void SetAnimationRange(sf::Vector2i newAnimationStartPoint, sf::Vector2i newAnimationEndPoint);
	void SetAnimationSpeed(float newTimePerFrame);
	void SetAnimationSpeedInSpritesPerSecond(int spritesPerSecond);
	
	void ResetAnimation();

	void SetSpriteSheet(std::string pathToSpriteSheet);
	void SetSpriteDimensions(sf::Vector2i newDimensions);

	sf::Vector2i GetAnimationStartingPoint();
	sf::Vector2i GetAnimationEndingPoint();

	sf::Sprite GetSprite();
	sf::Sprite* GetSpritePointer();
	sf::Texture GetSpriteSheet();

	void SetColor(sf::Color color);
	sf::Color GetColor();


private:
	// Methods
	void SetSpriteToPositionInSheet(sf::Vector2i positionInSheet);

	// Members
	// Sprite stuff
	// --------------------------------
	// The sprite sheet itself
	sf::Texture *m_spriteSheet;

	// True if the sprite has a texture.
	bool m_hasTexture;

	// The actual sprite that comes out at the end
	sf::Sprite m_sprite;

	// The dimensions of the final sprite
	sf::Vector2i m_spriteDimensions;
	
	// The x/y index of the starting sprite in the sheet
	sf::Vector2i m_startPositionInSheet;
	// The x/y index of the ending sprite in the sheet
	sf::Vector2i m_endPositionInSheet;

	sf::Vector2i m_currentPositionInSheet;

	// The amount of sprites per row and collumn.
	sf::Vector2i m_spriteSheetSpriteAmounts;

	// Animation
	// --------------------------------
	float m_timeBetweenFrames;

	float m_currentFrameTime;

};

#endif // __AnimatedSpriteH
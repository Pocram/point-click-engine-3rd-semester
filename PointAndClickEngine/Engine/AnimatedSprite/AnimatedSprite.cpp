#include "AnimatedSprite.h"

#include <Engine/File Manager/FileManager.h>
#include <Engine/File Manager/TextureManager.h>

AnimatedSprite::AnimatedSprite()
{
	m_spriteSheet = new sf::Texture();
	m_hasTexture = false;

	m_spriteDimensions = { 0, 0 };
	m_spriteSheetSpriteAmounts = { 0,0 };
	
	m_startPositionInSheet = { 0, 0 };
	m_endPositionInSheet = { 0, 0 };
	m_currentPositionInSheet = { 0, 0 };

	m_timeBetweenFrames = 0.f;
	m_currentFrameTime = 0.f;
}

AnimatedSprite::AnimatedSprite(std::string pathToSpriteSheet, sf::Vector2i spriteDimensions, sf::Vector2i startPositionInSheet, sf::Vector2i endPositionInSheet, float timeBetweenFrames)
{
	// Set defaults and stuff
	m_spriteDimensions = spriteDimensions;
	m_spriteSheetSpriteAmounts = { 0,0 };

	m_startPositionInSheet = startPositionInSheet;
	m_endPositionInSheet = endPositionInSheet;
	m_currentPositionInSheet = startPositionInSheet;

	m_timeBetweenFrames = timeBetweenFrames;
	m_currentFrameTime = 0.f;
	
	m_hasTexture = false;

	SetSpriteSheet(pathToSpriteSheet);
}

AnimatedSprite::~AnimatedSprite() {}


void AnimatedSprite::Update(sf::Time elapsedTime)
{
	// Count the time. Smart, eh?
	m_currentFrameTime += elapsedTime.asSeconds();

	// If the time of a frame has been reached, do stuff:
	if(m_currentFrameTime >= m_timeBetweenFrames)
	{
		// To get a (more or less) fluent animation going, we need to take care of the delta between the two values.
		// If the timer overshoots, we can just take the overshot value and use it as the new current one!
		m_currentFrameTime = m_timeBetweenFrames - m_currentFrameTime;

		// Advance to the next sprite
		m_currentPositionInSheet.x++;

		// Behaviour if the sprite is currently in the same row as the ending sprite
		if(m_currentPositionInSheet.y == m_endPositionInSheet.y)
		{
			if(m_currentPositionInSheet.x > m_endPositionInSheet.x)
			{
				m_currentPositionInSheet = m_startPositionInSheet;
			}
		} 
		// Behaviour if the sprite is not
		else
		{
			if (m_currentPositionInSheet.x >= m_spriteSheetSpriteAmounts.x) 
			{
				if (m_currentPositionInSheet.y < m_endPositionInSheet.y) 
				{
					m_currentPositionInSheet.x = 0;
					m_currentPositionInSheet.y++;
				} 
				else 
				{
					m_currentPositionInSheet = m_startPositionInSheet;
				}
			}
		}

		// Get new sprite from sheet
		SetSpriteToPositionInSheet(m_currentPositionInSheet);
	}
}

void AnimatedSprite::Draw(sf::RenderWindow* window)
{
	if(m_hasTexture)
	{
		window->draw(m_sprite);
	}
}


void AnimatedSprite::SetAnimationRange(sf::Vector2i newAnimationStartPoint, sf::Vector2i newAnimationEndPoint)
{
	// Skip if the new range is the same
	if (newAnimationStartPoint == m_startPositionInSheet && newAnimationEndPoint == m_endPositionInSheet)
		return;

	m_startPositionInSheet = newAnimationStartPoint;
	m_endPositionInSheet = newAnimationEndPoint;
	m_currentPositionInSheet = m_startPositionInSheet;

	m_currentFrameTime = 0.f;

	SetSpriteToPositionInSheet(m_currentPositionInSheet);
}

void AnimatedSprite::SetAnimationSpeed(float newTimePerFrame)
{
	m_timeBetweenFrames = newTimePerFrame;
}

void AnimatedSprite::SetAnimationSpeedInSpritesPerSecond(int spritesPerSecond)
{
	m_timeBetweenFrames = 1.f / static_cast<float>(spritesPerSecond);
}


void AnimatedSprite::ResetAnimation()
{
	m_currentPositionInSheet = m_startPositionInSheet;
	m_currentFrameTime = 0.f;
	
	SetSpriteToPositionInSheet(m_currentPositionInSheet);
}


void AnimatedSprite::SetSpriteSheet(std::string pathToSpriteSheet)
{
	m_spriteSheet = new sf::Texture();
	
	// Look for the file in the buffer
	std::string pathWithoutExt = pathToSpriteSheet;
	pathWithoutExt.resize(pathToSpriteSheet.find_last_of("."));

	sf::Texture* tex = TextureManager::GetTexture(pathWithoutExt);
	// TODO: Remove!
	if( false && tex != NULL)
	{
		std::size_t texSize = TextureManager::GetSizeOfTexture(pathWithoutExt);
		if (m_spriteSheet->loadFromMemory( tex, texSize) ) {
			m_hasTexture = true;
			m_spriteSheetSpriteAmounts = { static_cast<int>(m_spriteSheet->getSize().x) / m_spriteDimensions.x, static_cast<int>(m_spriteSheet->getSize().y) / m_spriteDimensions.y };

			m_sprite.setTexture(*m_spriteSheet);
			SetSpriteToPositionInSheet(m_currentPositionInSheet);
		}
	}
	// If it's not in the buffer, load it manually:
	else 
	{
		if (m_spriteSheet->loadFromFile(FileManager::GetPathToAsset(pathToSpriteSheet.c_str()))) {
			m_hasTexture = true;
			m_spriteSheetSpriteAmounts = { static_cast<int>(m_spriteSheet->getSize().x) / m_spriteDimensions.x, static_cast<int>(m_spriteSheet->getSize().y) / m_spriteDimensions.y };

			m_sprite.setTexture(*m_spriteSheet);
			SetSpriteToPositionInSheet(m_currentPositionInSheet);
		}
	}
}

void AnimatedSprite::SetSpriteDimensions(sf::Vector2i newDimensions)
{
	m_spriteDimensions = newDimensions;
	m_spriteSheetSpriteAmounts = { static_cast<int>(m_spriteSheet->getSize().x) / m_spriteDimensions.x, static_cast<int>(m_spriteSheet->getSize().y) / m_spriteDimensions.y };
	SetSpriteToPositionInSheet(m_currentPositionInSheet);
}


sf::Vector2i AnimatedSprite::GetAnimationStartingPoint()
{
	return m_startPositionInSheet;
}

sf::Vector2i AnimatedSprite::GetAnimationEndingPoint()
{
	return m_endPositionInSheet;
}


sf::Sprite AnimatedSprite::GetSprite()
{
	return m_sprite;
}

sf::Sprite* AnimatedSprite::GetSpritePointer() {
	return &m_sprite;
}

sf::Texture AnimatedSprite::GetSpriteSheet()
{
	return *m_spriteSheet;
}

void AnimatedSprite::SetColor(sf::Color color)
{
	m_sprite.setColor(color);
}

sf::Color AnimatedSprite::GetColor()
{
	return m_sprite.getColor();
}

void AnimatedSprite::SetSpriteToPositionInSheet(sf::Vector2i positionInSheet)
{
	int xPositionOfSprite = m_spriteDimensions.x * positionInSheet.x;
	int yPositionOfSprite = m_spriteDimensions.y * positionInSheet.y;
	sf::IntRect newSpriteRect(xPositionOfSprite, yPositionOfSprite, m_spriteDimensions.x, m_spriteDimensions.y);

	m_sprite.setTextureRect(newSpriteRect);
}
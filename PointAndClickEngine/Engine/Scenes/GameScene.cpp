#include "GameScene.h"

#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/File Manager/TextureManager.h>
#include <Engine/GameObjects/Interactables/InteractableObject.h>
#include <Engine/UI/GameInterface.h>

#include "boost/foreach.hpp"

extern GameScene CurrentScene;
extern sf::RenderWindow *GameWindow;

GameScene::GameScene()
{
	m_camera = new Camera();
	// Default values are the screen size
	if(GameWindow != NULL)
	{
		// 120px = height of UI rect
		m_bounds = sf::FloatRect(sf::Vector2f(0.0f, 0.0f), static_cast<sf::Vector2f>(GameWindow->getSize()) - sf::Vector2f(0.0f, 120.0f));
	}
	else
	{
		// Or nothing, if _somehow_ there is no window?
		m_bounds = sf::FloatRect();
	}

	m_playerObject = NULL;
}

GameScene::~GameScene()
{
	// Causes a null pointer exception :(
	//delete(m_camera);
}


void GameScene::Update(sf::Time elapsedTime)
{
	// Iterate through each game object backwards and handle input
	bool inputFound = false;
	bool inputFoundTotal = false;	// 'inputFound' may change over the course of the loop. 'inputFoundTotal' is true if ANY object returned true. This is used for determining an "empty" click.
	if(m_gameObjects.size() > 0)
	{
		std::list<GameObject*>::const_reverse_iterator gameObjectListReverseIterator;
		for (gameObjectListReverseIterator = m_gameObjects.rbegin(); gameObjectListReverseIterator != m_gameObjects.rend(); ++gameObjectListReverseIterator) {
			GameObject* go = (*gameObjectListReverseIterator);

			// Only check interactable objects
			if(!dynamic_cast<InteractableObject*>(go))
			{
				continue;
			}

			if (go->IsActive()) {
				inputFound = go->GetMouseEvents(inputFound);
				if(inputFound)
				{
					inputFoundTotal = true;
				}
			}
		}


		// If no object has been interacted with (hover, click, ...) run this ...
		if(!inputFoundTotal)
		{
			InteractableObject::SetCurrentlySelectedObject(NULL);

			// Tell player character to move towards click location
			if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
			{
				sf::Vector2i cursorPosition = sf::Mouse::getPosition(*GameWindow);
				// Check if cursor is within window and not on the UI bar.
				if (cursorPosition.x > 0 && cursorPosition.x < GameWindow->getSize().x
					&& cursorPosition.y > 0 && cursorPosition.y < GameWindow->getSize().y - GameUI::GetSize().y)
				{
					PlayerObject *player = GetPlayerObject();
					if(player != NULL)
					{
						sf::Vector2f worldPos = GetCamera().WindowToWorldPoint(cursorPosition);
						sf::Vector2f playerOrigin = sf::Vector2f(player->GetBoundingBox().width / 2.0f, player->GetBoundingBox().height);
						player->SetDestination(worldPos - playerOrigin);
					}
				}
			}
		}
	}

	// Iterate through each game object and update it if it's enabled.
	std::list<GameObject*>::const_iterator gameObjectListIterator;
	for (gameObjectListIterator = m_gameObjects.begin(); gameObjectListIterator != m_gameObjects.end(); ++gameObjectListIterator)
	{
		GameObject* go = (*gameObjectListIterator);

		if(go->IsActive())
		{
			go->Update(elapsedTime);
		}
	}

	// World UI Messages
	for (int i = 0; i < m_worldUiMessages.size(); i++) {
		m_worldUiMessages[i].RemoveFromDuration(elapsedTime.asSeconds());
	}

	// Screen UI Messages
	for (int i = 0; i < m_screenUiMessages.size(); i++) {
		m_screenUiMessages[i].RemoveFromDuration(elapsedTime.asSeconds());
	}

	// Remove any message that has a remaining duration of 0 or less
	m_worldUiMessages.erase(std::remove_if(m_worldUiMessages.begin(), m_worldUiMessages.end(), [](UiMessage i) {return i.GetDuration() <= 0.0f; }), m_worldUiMessages.end());
	m_screenUiMessages.erase(std::remove_if(m_screenUiMessages.begin(), m_screenUiMessages.end(), [](UiMessage i) {return i.GetDuration() <= 0.0f; }), m_screenUiMessages.end());

	// Update the camera afterwards to ensure accuracy.
	m_camera->Update(elapsedTime);
}

void GameScene::Draw(sf::RenderWindow* window)
{
	m_camera->Render(window, this);
}


GameObject* GameScene::InstantiateGameObject(GameObject* object)
{
	m_gameObjects.push_back(object);
	SortGameObjectsByDrawOrder();
	return object;
}


void GameScene::SortGameObjectsByDrawOrder()
{
	//printf("Sorting game objects!\n");
	m_gameObjects.sort(CompareGameObjectDrawOrder());
	//printf("Finished sorting!\n");
}


std::list<GameObject*> GameScene::GetGameObjects()
{
	return m_gameObjects;
}

std::vector<UiMessage> GameScene::GetWorldUiMessages()
{
	return m_worldUiMessages;
}

std::vector<UiMessage> GameScene::GetScreenUiMessages()
{
	return m_screenUiMessages;
}

void GameScene::Unload()
{
	printf("-- Unloading scene '%s'...\n", m_name.c_str());

	printf("Cleaning up %d GameObject(s)...\n", m_gameObjects.size());

	// Call Destroy() on each game object.
	std::list<GameObject*>::iterator gameObjectListIterator;
	for (gameObjectListIterator = m_gameObjects.begin(); gameObjectListIterator != m_gameObjects.end(); /* No increment is needed since erase() returns the next element, thus incrementing already. */) {
		GameObject* go = (*gameObjectListIterator);
		go->Destroy();
		
		gameObjectListIterator = m_gameObjects.erase(gameObjectListIterator);
		
		// Don't forget to delete the gameObject from memory
		delete(go);
	}


	printf("-- Finished unloading scene '%s'!\n", m_name.c_str());
}

std::string GameScene::GetName()
{
	return m_name;
}

Camera GameScene::GetCamera()
{
	return *m_camera;
}

void GameScene::SetCamera(Camera camera)
{
	m_camera = new Camera(camera);
}

sf::FloatRect GameScene::GetBounds()
{
	return m_bounds;
}

void GameScene::SetBounds(sf::FloatRect rect)
{
	m_bounds = rect;
}

void GameScene::LoadSceneDataImage(std::string imagePath) {
	sf::Texture *texture = TextureManager::GetTexture(imagePath);
	if (texture == NULL) {
		printf("Image for scene data at '%s' of scene '%s' could not be found.\n", imagePath.c_str(), GetName().c_str());
		return;
	}

	m_sceneDataImage = texture->copyToImage();
	printf("Loaded scene data image '%s'.\n", imagePath.c_str());
}

void GameScene::SetPlayerObject(PlayerObject* player)
{
	m_playerObject = player;
}

PlayerObject* GameScene::GetPlayerObject()
{
	return m_playerObject;
}

void GameScene::ShowTextInWorld(std::string message, sf::Vector2f worldPosition, float duration)
{
	UiMessage newMessage(message, worldPosition, duration);
	m_worldUiMessages.push_back(newMessage);
}

void GameScene::ShowTextOnScreen(std::string message, sf::Vector2f screenPosition, float duration, bool clampToScreen)
{
	UiMessage newMessage(message, screenPosition, duration);
	m_screenUiMessages.push_back(newMessage);
}

const sf::Image* GameScene::GetSceneDataImage() {
	return &m_sceneDataImage;
}


bool GameScene::AreBoundsInRedChannel(sf::FloatRect bounds, int threshold)
{
	const sf::Image* sceneData = GetSceneDataImage();
	if (sceneData == NULL)
		return false;

	// The bounds may only be within the image's  dimensions.
	if (bounds.left < 0 || (bounds.left + bounds.width) > sceneData->getSize().x
		|| bounds.top < 0 || (bounds.top + bounds.height) > sceneData->getSize().y)
		return false;

	// Iterate over every pixel of the bounds.
	for (int x = bounds.left; x < (bounds.left + bounds.width); x++) {
		for (int y = bounds.top; y < (bounds.top + bounds.height); y++) {
			// Check for every pixel if the red channel is white.
			// If it, the bounds are not completely inside.
			if (sceneData->getPixel(x, y).r < threshold)
				return false;
		}
	}

	return true;
}

bool GameScene::AreBoundsInGreenChannel(sf::FloatRect bounds, int threshold)
{
	const sf::Image* sceneData = GetSceneDataImage();
	if (sceneData == NULL)
		return false;

	// The bounds may only be within the image's  dimensions.
	if (bounds.left < 0 || (bounds.left + bounds.width) > sceneData->getSize().x
		|| bounds.top < 0 || (bounds.top + bounds.height) > sceneData->getSize().y)
		return false;

	// Iterate over every pixel of the bounds.
	for (int x = bounds.left; x < (bounds.left + bounds.width); x++) {
		for (int y = bounds.top; y < (bounds.top + bounds.height); y++) {
			// Check for every pixel if the red channel is white.
			// If it, the bounds are not completely inside.
			if (sceneData->getPixel(x, y).g < threshold)
				return false;
		}
	}

	return true;
}

bool GameScene::AreBoundsInBlueChannel(sf::FloatRect bounds, int threshold)
{
	const sf::Image* sceneData = GetSceneDataImage();
	if (sceneData == NULL)
		return false;

	// The bounds may only be within the image's  dimensions.
	if (bounds.left < 0 || (bounds.left + bounds.width) > sceneData->getSize().x
		|| bounds.top < 0 || (bounds.top + bounds.height) > sceneData->getSize().y)
		return false;

	// Iterate over every pixel of the bounds.
	for (int x = bounds.left; x < (bounds.left + bounds.width); x++) {
		for (int y = bounds.top; y < (bounds.top + bounds.height); y++) {
			// Check for every pixel if the red channel is white.
			// If it, the bounds are not completely inside.
			if (sceneData->getPixel(x, y).b < threshold)
			{
				return false;
			}
		}
	}

	return true;
}
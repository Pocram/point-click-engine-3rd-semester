#ifndef __GameScene
#define __GameScene

#include <list>
#include <vector>
#include <Engine/GameObjects/GameObject.h>
#include <Engine/GameObjects/Player/PlayerObject.h>
#include <Engine/UI/UI Messages/UiMessage.h>

class Camera;

class GameScene {

public:
	// Constructors
	GameScene();
	~GameScene();


	// Loop Methods
	// Updates the scene.
	void Update(sf::Time elapsedTime);
	// Draws the scene.
	void Draw(sf::RenderWindow *window);

	// Methods
	// Adds the given object to the list of objects. This enables it to be called by update, draw and find functions.
	GameObject *InstantiateGameObject(GameObject *object);
	// Sorts the game objects by their draw order. Objects with a higher draw index are drawn later.
	void SortGameObjectsByDrawOrder();

	std::list<GameObject*> GetGameObjects();
	std::vector<UiMessage> GetWorldUiMessages();
	std::vector<UiMessage> GetScreenUiMessages();

	// Unloads the scene, destroying each object.
	void Unload();

	std::string GetName();

	Camera GetCamera();
	void SetCamera(Camera camera);

	void SetBounds(sf::FloatRect rect);
	sf::FloatRect GetBounds();

	const sf::Image* GetSceneDataImage();

	bool AreBoundsInRedChannel(sf::FloatRect bounds, int threshold = 255);
	bool AreBoundsInGreenChannel(sf::FloatRect bounds, int threshold = 255);
	bool AreBoundsInBlueChannel(sf::FloatRect bounds, int threshold = 255);

	PlayerObject* GetPlayerObject();

	// Places a message in the world. Camera movement affects its position on the screen.
	void ShowTextInWorld(std::string message, sf::Vector2f worldPosition, float duration = 5.0f);

	// Places a message on the screen. Camera movement does not affect its position.
	void ShowTextOnScreen(std::string message, sf::Vector2f screenPosition, float duration = 5.0f, bool clampToScreen = false);

protected:
	std::string m_name;

	void LoadSceneDataImage(std::string imagePath);

	void SetPlayerObject(PlayerObject *player);

private:
	// Members
	std::list<GameObject*> m_gameObjects;
	
	sf::FloatRect m_bounds;
	
	sf::Image m_sceneDataImage;

	// The camera is a pointer because the classes depend on each other and C++ is so bad it can't comprehend it otherwise.
	// If only there was a language that could. Now that would be quite 'sharp'.
	Camera *m_camera;

	PlayerObject *m_playerObject;


	// Messages
	// ------------------------
	std::vector<UiMessage> m_worldUiMessages;
	std::vector<UiMessage> m_screenUiMessages;
};

#endif // __GameScene
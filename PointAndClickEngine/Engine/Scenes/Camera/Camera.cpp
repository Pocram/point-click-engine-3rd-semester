#include "Camera.h"

#include <list>
#include <Engine/GameObjects/GameObject.h>
#include <Engine/Scenes/GameScene.h>
#include <Engine/UI/GameInterface.h>

#include "boost/foreach.hpp"

// Constructors
Camera::Camera() 
{
	if (GameWindow != NULL) 
	{
		m_bounds.width = GameWindow->getSize().x;
		m_bounds.height = GameWindow->getSize().y;

		m_transform.setOrigin(m_bounds.width / 2.f, m_bounds.height / 2.f);
	}
	else
	{
		printf("The global field 'GameWindow' has not been created, yet. Using default values ... (If this is the first time you are seeing this, don't worry. It's normal.)\n");
	}
}

Camera::~Camera() {}


void Camera::Update(sf::Time elapsedTime)
{
	PlayerObject *player = CurrentScene.GetPlayerObject();
	if (player == NULL)
		return;

	CenterOnPosition(player->GetCameraAnchorPosition());
	//printf("C: %f, %f\tP: %f, %f\n", GetPosition().x, GetPosition().y, player->GetTransform().getPosition().x, player->GetTransform().getPosition().y);
}


void Camera::Render(sf::RenderWindow* window, GameScene *scene)
{
	// Get all game objects in the given scene
	std::list<GameObject*> gameObjectsInScene = scene->GetGameObjects();

	// Iterate through them ...
	std::list<GameObject*>::const_iterator gameObjectListIterator;
	for (gameObjectListIterator = gameObjectsInScene.begin(); gameObjectListIterator != gameObjectsInScene.end(); ++gameObjectListIterator)
	{
		GameObject* go = (*gameObjectListIterator);
		// Only draw when object is active and within camera frustrum
		if (go->IsActive()) 
		{
			if (m_bounds.intersects(go->GetBoundingBox()) || true) 
			{
				// Calculate the position on the screen
				sf::Transformable drawTransform = go->GetTransform();
				drawTransform.setPosition(drawTransform.getPosition().x - GetPosition().x, drawTransform.getPosition().y - GetPosition().y);

				// And draw it! <3
				go->Draw(window, drawTransform);
			}
		}
	}

	// World UI Messages
	BOOST_FOREACH(UiMessage message, scene->GetWorldUiMessages())
	{
		sf::Text text = message.GetTextComponent();
		text.setColor(sf::Color::Black);

		// Draw the text 4 additional times to create an outline
		text.setPosition(message.GetTextComponent().getPosition() - GetPosition() + sf::Vector2f(0, 2));
		window->draw(text);

		text.setPosition(message.GetTextComponent().getPosition() - GetPosition() + sf::Vector2f(2, 0));
		window->draw(text);

		text.setPosition(message.GetTextComponent().getPosition() - GetPosition() + sf::Vector2f(0, -2));
		window->draw(text);

		text.setPosition(message.GetTextComponent().getPosition() - GetPosition() + sf::Vector2f(-2, 0));
		window->draw(text);

		text = message.GetTextComponent();
		text.setPosition(message.GetTextComponent().getPosition() - GetPosition());
		window->draw(text);
	}

	// Screen UI Messages
	BOOST_FOREACH(UiMessage message, scene->GetScreenUiMessages()) {
		sf::Text text = message.GetTextComponent();
		text.setColor(sf::Color::Black);

		// Draw the text 4 additional times to create an outline
		text.setPosition(message.GetTextComponent().getPosition() + sf::Vector2f(0, 2));
		window->draw(text);

		text.setPosition(message.GetTextComponent().getPosition() + sf::Vector2f(2, 0));
		window->draw(text);

		text.setPosition(message.GetTextComponent().getPosition() + sf::Vector2f(0, -2));
		window->draw(text);

		text.setPosition(message.GetTextComponent().getPosition() + sf::Vector2f(-2, 0));
		window->draw(text);

		text = message.GetTextComponent();
		text.setPosition(message.GetTextComponent().getPosition());
		window->draw(text);
	}

	return;

	// Overlay scene data for debug purposes
	const sf::Image *sceneData = scene->GetSceneDataImage();
	if(sceneData != NULL)
	{
		sf::Texture dataTexture;
		dataTexture.loadFromImage(*sceneData);
		sf::Sprite sprite;
		sprite.setTexture(dataTexture);
		sprite.setColor(sf::Color(255, 255, 255, 255 / 4));
		sprite.setPosition(-GetPosition());

		window->draw(sprite);
	}
}


// Methods
sf::Vector2f Camera::GetPosition()
{
	return m_transform.getPosition();
}

void Camera::SetPosition(sf::Vector2f newPosition)
{
	const sf::Image *sceneData = CurrentScene.GetSceneDataImage();
	if(sceneData == NULL)
	{
		m_transform.setPosition(newPosition);
		UpdateBounds();
		return;
	}

	// Only one axis is supported now.
	newPosition.y = 0.0f;

	// Clamp to scene data
	if (newPosition.x < 0.0f)
		newPosition.x = 0.0f;

	if ((newPosition.x + GetBounds().width) > sceneData->getSize().x)
		newPosition.x = sceneData->getSize().x - GetBounds().width;

	m_transform.setPosition(newPosition);
	UpdateBounds();
}

void Camera::CenterOnPosition(sf::Vector2f position)
{
	sf::Vector2f size = sf::Vector2f(GetBounds().width, GetBounds().height);
	SetPosition(position - size * 0.5f);
}

void Camera::Translate(sf::Vector2f motionDelta)
{
	SetPosition(sf::Vector2f{ m_transform.getPosition().x + motionDelta.x, m_transform.getPosition().y + motionDelta.y });
}

sf::FloatRect Camera::GetBounds()
{
	return m_bounds;
}

sf::Vector2f Camera::ScreenToWorldPoint(sf::Vector2f screenPoint, bool clampScreenPointBetween0And1, bool excludeUi)
{
	if(clampScreenPointBetween0And1)
	{
		if (screenPoint.x < 0.f)
			screenPoint.x = 0.f;
		else if (screenPoint.x > 1.f)
			screenPoint.x = 1.f;

		if (screenPoint.y < 0.f)
			screenPoint.y = 0.f;
		else if (screenPoint.y > 1.f)
			screenPoint.y = 1.f;
	}

	sf::Vector2f size = sf::Vector2f(m_bounds.width, m_bounds.height);
	if(excludeUi)
	{
		size.y -= GameUI::GetSize().y;
	}

	return sf::Vector2f{ m_bounds.left, m_bounds.top } + sf::Vector2f{ screenPoint.x * size.x, screenPoint.y * size.y};
}

sf::Vector2f Camera::ScreenToWindowPoint(sf::Vector2f screenPoint, bool clampScreenPointBetween0And1, bool excludeUi)
{
	if (clampScreenPointBetween0And1) {
		if (screenPoint.x < 0.f)
			screenPoint.x = 0.f;
		else if (screenPoint.x > 1.f)
			screenPoint.x = 1.f;

		if (screenPoint.y < 0.f)
			screenPoint.y = 0.f;
		else if (screenPoint.y > 1.f)
			screenPoint.y = 1.f;
	}

	sf::Vector2f size = sf::Vector2f(m_bounds.width, m_bounds.height);
	if (excludeUi) {
		size.y -= GameUI::GetSize().y;
	}

	return sf::Vector2f(screenPoint.x * size.x, screenPoint.y * size.y);
}

sf::Vector2f Camera::WorldToScreenPoint(sf::Vector2f worldPoint, bool clampOutputBetween0And1)
{
	sf::Vector2f output;


	output.x = (worldPoint.x - m_bounds.left) / m_bounds.width;
	output.y = (worldPoint.y - m_bounds.top) / m_bounds.height;
	
	// Inverse linear interpolation here
	if(clampOutputBetween0And1)
	{
		if (output.x < 0.f)
			output.x = 0.f;
		else if (output.x > 1.f)
			output.x = 1.f;

		if (output.y < 0.f)
			output.y = 0.f;
		else if (output.y > 1.f)
			output.y = 1.f;
	}

	return output;
}

sf::Vector2f Camera::WindowToWorldPoint(sf::Vector2i windowPoint, bool clamlInputToScreen)
{
	if (GameWindow == NULL)
		return sf::Vector2f(0.0f, 0.0f);

	sf::Vector2f relativeCursorPosition = { static_cast<float>(windowPoint.x) / GameWindow->getSize().x, static_cast<float>(windowPoint.y) / GameWindow->getSize().y };
	return ScreenToWorldPoint(relativeCursorPosition, clamlInputToScreen);
}

bool Camera::IsWorldPointOnScreen(sf::Vector2f worldPoint)
{
	if (worldPoint.x < m_bounds.left)
		return false;

	if (worldPoint.x > m_bounds.left + m_bounds.width)
		return false;

	if (worldPoint.y < m_bounds.top)
		return false;

	if (worldPoint.y > m_bounds.top + m_bounds.height)
		return false;

	return true;
}


void Camera::UpdateBounds()
{
	m_bounds.left = GetPosition().x;// -m_transform.getOrigin().x;
	m_bounds.top = GetPosition().y;// -m_transform.getOrigin().y;
}
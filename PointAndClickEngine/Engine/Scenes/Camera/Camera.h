#ifndef __CameraHead
#define __CameraHead

#include "SFML/Graphics.hpp"

class GameScene;

extern sf::RenderWindow *GameWindow;
extern GameScene CurrentScene;

class Camera {

public:
	// Constructors
	Camera();
	virtual ~Camera();

	
	// Methods
	// Loop
	virtual void Update(sf::Time elapsedTime);
	void Render(sf::RenderWindow *window, GameScene *scene);

	// Transform
	sf::Vector2f GetPosition();
	void SetPosition(sf::Vector2f newPosition);
	void CenterOnPosition(sf::Vector2f position);
	void Translate(sf::Vector2f motionDelta);

	sf::FloatRect GetBounds();

	// Conversions
	sf::Vector2f ScreenToWorldPoint(sf::Vector2f screenPoint, bool clampScreenPointBetween0And1 = true, bool ignoreUi = false);
	sf::Vector2f ScreenToWindowPoint(sf::Vector2f screenPoint, bool clampScreenPointBetween0And1 = true, bool excludeUi = true);
	sf::Vector2f WorldToScreenPoint(sf::Vector2f worldPoint, bool clampOutputBetween0And1 = true);
	sf::Vector2f WindowToWorldPoint(sf::Vector2i windowPoint, bool clamlInputToScreen = true);
	
	bool IsWorldPointOnScreen(sf::Vector2f worldPoint);



private:
	// Methods
	void UpdateBounds();

	// Members
	sf::Transformable m_transform;
	sf::FloatRect m_bounds;

};

#endif // __CameraHead
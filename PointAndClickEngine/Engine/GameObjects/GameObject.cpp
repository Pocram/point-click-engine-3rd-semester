#include "GameObject.h"
#include <Engine/Scenes/GameScene.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/UI/GameInterface.h>

#include "boost/foreach.hpp"
#include <Engine/Items/Inventory.h>
#include "Interactables/InteractableObject.h"
#include <Util/VectorMath.h>

extern GameScene CurrentScene;
extern sf::RenderWindow *GameWindow;


GameObject::GameObject()
{
	m_Name = "GameObject";

	m_isActive = true;

	SetDrawOrder(0);

	m_cursorIsHoveringObjectPreviousFrame = false;
	m_cursorIsHoveringObject = false;

	m_cursorIsDownOnObject = false;
	m_cursorIsDownOnObjectPreviousFrame = false;
}

GameObject::~GameObject() {}


// Public
// Game loop and object cycle
void GameObject::Update(sf::Time elapsedTime)
{
	OnUpdate(elapsedTime);
	
	m_previousPosition = GetTransform().getPosition();
	m_previousDeltaTime = elapsedTime;
}

bool GameObject::GetMouseEvents(bool someObjectWasHandled)
{
	bool inputDetected = false;

	m_cursorIsHoveringObjectPreviousFrame = m_cursorIsHoveringObject;
	m_cursorIsDownOnObjectPreviousFrame = m_cursorIsDownOnObject;

	// Cursor checks
	Camera camera = CurrentScene.GetCamera();
	// Get the cursor position relative to the window in pixels
	sf::Vector2i cursorPosition = sf::Mouse::getPosition(*GameWindow);
	
	sf::Vector2f relativeCursorPosition(-1.f, -1.f);
	sf::Vector2f cursorWorldPosition(-1.f, -1.f);

	// Check if cursor is within window and not on the UI bar.
	if (cursorPosition.x < 0 || cursorPosition.x > GameWindow->getSize().x
		|| cursorPosition.y < 0 || cursorPosition.y > GameWindow->getSize().y - GameUI::GetSize().y) 
	{
		// Cursor is outside of window
		m_cursorIsHoveringObject = false;
		m_cursorIsDownOnObject = false;
	} 
	else 
	{
		// Cursor is on window

		// Convert the pixel-coordinates to percentages based on the window size
		relativeCursorPosition = { static_cast<float>(cursorPosition.x) / GameWindow->getSize().x, static_cast<float>(cursorPosition.y) / GameWindow->getSize().y };
		// Convert to world position
		cursorWorldPosition = camera.ScreenToWorldPoint(relativeCursorPosition);

		if (m_boundingRect.contains(cursorWorldPosition)) 
		{
			m_cursorIsHoveringObject = true;

			// Get
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				m_cursorIsDownOnObject = true;
			}
			else
			{
				m_cursorIsDownOnObject = false;
			}
		} else 
		{
			m_cursorIsHoveringObject = false;
			m_cursorIsDownOnObject = false;
		}
	}


	// Hover enter
	if (!someObjectWasHandled && m_cursorIsHoveringObject && !m_cursorIsHoveringObjectPreviousFrame)
	{
		OnMouseEnter(cursorWorldPosition);
		inputDetected = true;
	} 
	// Hover stay
	else if (!someObjectWasHandled && m_cursorIsHoveringObject && m_cursorIsHoveringObjectPreviousFrame)
	{
		OnMouseHover(cursorWorldPosition);
		inputDetected = true;

		//InteractableObject::SetCurrentlySelectedObject(this);
	} 
	// Hover exit
	else if (!m_cursorIsHoveringObject && m_cursorIsHoveringObjectPreviousFrame) 
	{
		OnMouseExit(cursorWorldPosition);
	}


	// Click
	if (!someObjectWasHandled && m_cursorIsDownOnObject && !m_cursorIsDownOnObjectPreviousFrame) {
		printf("%s\n", m_Name.c_str());
		OnMouseDown(cursorWorldPosition);
		inputDetected = true;

		if(Inventory::GetCurrentlySelectedItem() != NULL)
		{
			Inventory::UnselectItem();
		}
	}
	// Hold
	else if (!someObjectWasHandled && m_cursorIsDownOnObject && m_cursorIsDownOnObjectPreviousFrame) {
		OnMouseHold(cursorWorldPosition);
		inputDetected = true;
	}
	// Release
	else if (!m_cursorIsDownOnObject && m_cursorIsDownOnObjectPreviousFrame) {
		OnMouseRelease(cursorWorldPosition);
		inputDetected = true;
	}

	if(!inputDetected && !someObjectWasHandled)
	{
		//InteractableObject::SetCurrentlySelectedObject(NULL);
	}

	return inputDetected;
}

void GameObject::Draw(sf::RenderWindow *window, sf::Transformable transform)
{
}

void GameObject::Destroy()
{
	printf("Destroying GameObject '%s'...\n", m_Name.c_str());
	OnDestroy();
}


// Activity
bool GameObject::IsActive()
{
	return m_isActive;
}

void GameObject::SetActive(bool active)
{
	bool wasActive = m_isActive;
	m_isActive = active;

	if(active && !wasActive)
	{
		OnEnable();
	} 
	else if(!active && wasActive)
	{
		OnDisable();
	}
}


// Sorting and rendering
int GameObject::GetDrawOrder()
{
	return m_drawOrder;
}

void GameObject::SetDrawOrder(int newIndex)
{
	m_drawOrder = newIndex;
	CurrentScene.SortGameObjectsByDrawOrder();
}

void GameObject::SetVisual()
{
}


// Transformation
sf::Transformable GameObject::GetTransform()
{
	return m_transform;
}

void GameObject::SetTransform(sf::Transformable newTransform)
{
	m_transform = newTransform;
	CalculateBoundingBox();
}

void GameObject::Translate(sf::Vector2f translationDelta, bool ignoreSceneData)
{
	sf::Vector2f newPos = m_transform.getPosition() + translationDelta;
	//m_transform.setPosition(newPos);
	SetPosition(newPos, ignoreSceneData);
}


void GameObject::SetPosition(sf::Vector2f newPosition, bool ignoreSceneData)
{
	if(!ignoreSceneData)
	{
		sf::FloatRect newBounds = sf::FloatRect(newPosition, sf::Vector2f(m_boundingRect.width, m_boundingRect.height));

		// Only allow movement if the new position is within the "navmesh".
		if (!CurrentScene.AreBoundsInRedChannel(newBounds))
			return;	
	}

	m_transform.setPosition(newPosition);
	CalculateBoundingBox();
}

void GameObject::SetScale(sf::Vector2f newScale)
{
	m_transform.setScale(newScale);
	CalculateBoundingBox();
}

void GameObject::SetOrigin(sf::Vector2f newOrigin)
{
	m_transform.setOrigin(newOrigin);
	CalculateBoundingBox();
}

void GameObject::SetRotation(float newAngle)
{
	m_transform.setRotation(newAngle);
	CalculateBoundingBox();
}


// Protected
// Bounds
void GameObject::CalculateBoundingBox()
{
}

void GameObject::CalculateBoundingBoxFromSprite(sf::Sprite sprite) {
	sprite.setOrigin(m_transform.getOrigin());
	sprite.setPosition(m_transform.getPosition());
	sprite.setScale(m_transform.getScale());
	sprite.setRotation(m_transform.getRotation());

	m_boundingRect = sprite.getGlobalBounds();
}


sf::FloatRect GameObject::GetBoundingBox()
{
	return m_boundingRect;
}

sf::Vector2f GameObject::GetVelocity()
{
	sf::Vector2f delta = GetTransform().getPosition() - m_previousPosition;
	return delta / m_previousDeltaTime.asSeconds();
}

float GameObject::GetDistanceFromPlayer()
{
	PlayerObject *player = CurrentScene.GetPlayerObject();
	if (player == NULL)
		return 0.0f;

	sf::Vector2f centerOfThis = GetTransform().getPosition() + 0.5f * sf::Vector2f(GetBoundingBox().width, GetBoundingBox().height);
	sf::Vector2f direction = player->GetFeetPosition() - centerOfThis;

	return VectorMath::Magnitude(direction);
}

void GameObject::DrawBounds(sf::RenderWindow* window, sf::Transformable transform)
{
	sf::RectangleShape boundsShape;
	boundsShape.setSize(sf::Vector2f{ m_boundingRect.width, m_boundingRect.height });

	boundsShape.setFillColor(sf::Color{ 0, 0, 0, 0 });
	boundsShape.setOutlineColor(sf::Color{ 0, 255, 0, 200 });
	boundsShape.setOutlineThickness(-1.f);

	sf::Transformable renderTransform;
	renderTransform.setPosition(transform.getPosition());

	window->draw(boundsShape, renderTransform.getTransform());
}


GameObject* GameObject::FindGameObject(std::string name)
{
	std::list<GameObject*> objectsInScene = CurrentScene.GetGameObjects();
	BOOST_FOREACH(GameObject* go, objectsInScene)
	{
		if(go->m_Name == name)
		{
			return go;
		}
	}

	return NULL;
}


void GameObject::OnUpdate(sf::Time elapsedTime)
{
}


void GameObject::OnMouseEnter(sf::Vector2f cursorPosition)
{
}

void GameObject::OnMouseHover(sf::Vector2f cursorPosition)
{
}

void GameObject::OnMouseExit(sf::Vector2f cursorPosition)
{
}

void GameObject::OnMouseDown(sf::Vector2f cursorPosition)
{
}

void GameObject::OnMouseHold(sf::Vector2f cursorPosition)
{
}

void GameObject::OnMouseRelease(sf::Vector2f cursorPosition)
{
}


// Mono behaviour
void GameObject::OnEnable()
{
}

void GameObject::OnDisable()
{
}

void GameObject::OnDestroy()
{
}
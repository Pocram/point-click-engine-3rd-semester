#include "InteractableObject.h"
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Items/Inventory.h>

// PUBLIC
// =======================================================
// Constructors
// ------------------------------
InteractableObject::InteractableObject() 
{
	m_canBeLookedAt = false;
	m_canBePickedUp = false;
	m_canBeUsed = false;
}

InteractableObject::~InteractableObject() {}


// Methods
// ------------------------------
// Conditionals
bool InteractableObject::CanBeLookedAt()
{
	return false;
}

bool InteractableObject::CanBeUsed(Item* item)
{
	return false;
}

bool InteractableObject::CanBePickedUp()
{
	return false;
}


void InteractableObject::LookAt()
{
	printf("Looking at object '%s' - Reset interaction mode\n", m_Name.c_str());
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void InteractableObject::UseWith(Item* item)
{
	printf("Using object '%s' - Reset interaction mode\n", m_Name.c_str());
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

void InteractableObject::PickUp()
{
	printf("Picking up object '%s' - Reset interaction mode\n", m_Name.c_str());
	GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
}

GameObject* InteractableObject::GetCurrentlySelectedObject()
{
	return m_currentlyHoveredGameObject;
}

void InteractableObject::SetCurrentlySelectedObject(GameObject* gO)
{
	m_currentlyHoveredGameObject = gO;
}

// PROTECTED
// =======================================================
// Methods
// ------------------------------
void InteractableObject::OnMouseDown(sf::Vector2f cursorPosition)
{
	printf("Clicked on object '%s' with interaction mode '%s'.\n", m_Name.c_str(), GameLogic::GetInteractionModeString(GameLogic::GetCurrentInteractionMode()).c_str());

	switch (GameLogic::GetCurrentInteractionMode()) 
	{
	case GameLogic::None:
		return;
	case GameLogic::LookAt:
		LookAt();
		break;
	case GameLogic::Use:
		UseWith(Inventory::GetCurrentlySelectedItem());
		break;
	case GameLogic::PickUp:
		PickUp();
		break;
	default:
		break;
	}
}

void InteractableObject::OnMouseEnter(sf::Vector2f cursorPosition)
{
}

void InteractableObject::OnMouseHover(sf::Vector2f cursorPosition)
{
	SetCurrentlySelectedObject(this);
}

void InteractableObject::OnMouseExit(sf::Vector2f cursorPosition)
{
}

// PRIVATE
// =======================================================
// Static members
// ------------------------------
GameObject* InteractableObject::m_currentlyHoveredGameObject = NULL;
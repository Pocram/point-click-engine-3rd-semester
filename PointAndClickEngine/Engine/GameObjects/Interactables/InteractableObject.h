#ifndef __InteractableObjectH
#define __InteractableObjectH

#include <Engine/GameObjects/GameObject.h>
#include <Engine/Items/Item.h>

class InteractableObject : public GameObject {

public:
	// Constructors
	InteractableObject();
	virtual ~InteractableObject();

	// Methods
	// Conditionals
	virtual bool CanBeLookedAt();
	virtual bool CanBeUsed(Item* item);
	virtual bool CanBePickedUp();

	// Interactions
	virtual void LookAt();
	virtual void UseWith(Item* item);
	virtual void PickUp();

	static GameObject* GetCurrentlySelectedObject();
	static void SetCurrentlySelectedObject(GameObject* gO);

protected:
	// Mouse clicking events
	void OnMouseDown(sf::Vector2f cursorPosition) override;

	void OnMouseEnter(sf::Vector2f cursorPosition) override;
	void OnMouseHover(sf::Vector2f cursorPosition) override;
	void OnMouseExit(sf::Vector2f cursorPosition) override;

	// Members
	bool m_canBeLookedAt;
	bool m_canBeUsed;
	bool m_canBePickedUp;

private:
	static GameObject* m_currentlyHoveredGameObject;

};

#endif // __InteractableObjectH
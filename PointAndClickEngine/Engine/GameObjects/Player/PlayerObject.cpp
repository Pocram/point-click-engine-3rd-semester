#include "PlayerObject.h"
#include <Util/VectorMath.h>

PlayerObject::PlayerObject()
{
	m_movementSpeed = 200.0f;
	m_destinationDistanceThreshold = 5.0f;

	m_animSprite = AnimatedSprite("Sprites/playerSheet.png", { 64, 64 }, { 0, 0 }, { 2, 0 }, 0.5f);
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());

	m_destination = sf::Vector2f();
}

PlayerObject::~PlayerObject() {}


void PlayerObject::SetDestination(sf::Vector2f position)
{
	m_destination = position;
}

sf::Vector2f PlayerObject::GetDestination()
{
	return m_destination;
}

sf::Vector2f PlayerObject::GetFeetPosition()
{
	sf::Vector2f offset = sf::Vector2f(GetBoundingBox().width / 2.0f, GetBoundingBox().height);
	return GetTransform().getPosition() + offset;
}

sf::Vector2f PlayerObject::GetCameraAnchorPosition() {
	sf::Vector2f offset = sf::Vector2f(GetBoundingBox().width / 2.0f, GetBoundingBox().height / 4.0f);
	return GetTransform().getPosition() + offset;
}


void PlayerObject::OnUpdate(sf::Time elapsedTime)
{
	MoveTowardsDestination(elapsedTime);
	m_animSprite.Update(elapsedTime);

	// Change animation depending on velocity.
	sf::Vector2f velocity = GetVelocity();
	if(velocity.x < 0.0f)
	{
		// Go left
		m_animSprite.SetAnimationRange(sf::Vector2i(6, 0), sf::Vector2i(8, 0));
		m_animSprite.SetAnimationSpeed(0.125f);
	}
	else if(velocity.x > 0.0f)
	{
		// Go right
		m_animSprite.SetAnimationRange(sf::Vector2i(3, 0), sf::Vector2i(5, 0));
		m_animSprite.SetAnimationSpeed(0.125f);
	}
	else
	{
		// Stand still
		m_animSprite.SetAnimationRange(sf::Vector2i(0, 0), sf::Vector2i(2, 0));
		m_animSprite.SetAnimationSpeed(0.5f);
	}
}

void PlayerObject::Draw(sf::RenderWindow* window, sf::Transformable transform)
{
	window->draw(m_animSprite.GetSprite(), transform.getTransform());
}

void PlayerObject::CalculateBoundingBox()
{
	CalculateBoundingBoxFromSprite(m_animSprite.GetSprite());
}



void PlayerObject::MoveTowardsDestination(sf::Time elapsedTime)
{
	sf::Vector2f direction = m_destination - GetTransform().getPosition();
	float magnitude = VectorMath::Magnitude(direction);

	// Check if the player is close enough to their destination. If so, stop.
	if(magnitude <= m_destinationDistanceThreshold)
	{
		return;
	}
	
	// Normalize direction to ensure constant speed
	direction = VectorMath::Normalized(direction);
	Translate(direction * m_movementSpeed * elapsedTime.asSeconds());
}
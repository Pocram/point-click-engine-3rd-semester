#ifndef __PlayerObjectH
#define __PlayerObjectH

#include <Engine/GameObjects/GameObject.h>
#include <Engine/AnimatedSprite/AnimatedSprite.h>

class PlayerObject : public GameObject {

public:
	// Constructors
	PlayerObject();
	~PlayerObject();

	// Methods
	void SetDestination(sf::Vector2f position);
	sf::Vector2f GetDestination();

	sf::Vector2f GetFeetPosition();
	sf::Vector2f GetCameraAnchorPosition();

protected:
	virtual void OnUpdate(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform) override;

	virtual void CalculateBoundingBox() override;


private:
	// Methods
	void MoveTowardsDestination(sf::Time elapsedTime);

	float m_movementSpeed;
	float m_destinationDistanceThreshold;

	sf::Vector2f m_destination;


	AnimatedSprite m_animSprite;

};

#endif // __PlayerObjectH
#ifndef __GameObject
#define __GameObject

#include <string>
#include <SFML/Graphics.hpp>

class GameObject {

public:
	// Constructors
	// ==============================================================================================
	GameObject();
	virtual ~GameObject();



	// Methods
	// ==============================================================================================
	virtual void Update(sf::Time elapsedTime);
	virtual void Draw(sf::RenderWindow *window, sf::Transformable transform);
	// Destroy the Game Object. Also calls the virtual OnDestroy().
	void Destroy();
	// Handles mouse hovering and clicking. Returns true if hover or click is detected
	bool GetMouseEvents(bool someObjectWasHandled);

	// Activity
	// ----------------------------------------------------------------------------------------------
	// A disabled object will not be updated or drawn to the screen
	// Returns 'true' if the object is active.
	bool IsActive();
	// Sets the object's activity to the given value.
	void SetActive(bool active);

	// Sorting and rendering
	// ----------------------------------------------------------------------------------------------
	// Objects with a higher index are drawn later (on top)
	// Returns the object's draw order index.
	int GetDrawOrder();
	// Sets the object's draw order index to the given value.
	void SetDrawOrder(int newIndex);

	// Sets the object's appearance. OBSOLETE?
	virtual void SetVisual();

	// Transformation
	// ----------------------------------------------------------------------------------------------
	// Returns the object's transform.
	sf::Transformable GetTransform();
	// Sets the object's transform.
	void SetTransform(sf::Transformable newTransform);

	// Moves the object into the given direction.
	void Translate(sf::Vector2f translationDelta, bool ignoreSceneData = false);
	
	// Sets the position of the object.
	void SetPosition(sf::Vector2f newPosition, bool ignoreSceneData = false);
	// Sets the scale of the object.
	void SetScale(sf::Vector2f newScale);
	// Sets the origin of the object.
	void SetOrigin(sf::Vector2f newOrigin);
	// Sets the rotation of the object.
	void SetRotation(float newAngle);

	// Returns the bounding box of the object.
	sf::FloatRect GetBoundingBox();


	sf::Vector2f GetVelocity();


	float GetDistanceFromPlayer();



	// Members
	// ==============================================================================================
	std::string m_Name;


	// Debugging
	void DrawBounds(sf::RenderWindow *window, sf::Transformable transform);



protected:
	sf::FloatRect m_boundingRect;

	// Finds a gameObject in the current scene by its name and returns a pointer
	GameObject* FindGameObject(std::string name);

	// MONOBEHAVIOUR!
	virtual void OnUpdate(sf::Time elapsedTime);

	// Mouse hovering events
	virtual void OnMouseEnter(sf::Vector2f cursorPosition);
	virtual void OnMouseHover(sf::Vector2f cursorPosition);
	virtual void OnMouseExit(sf::Vector2f cursorPosition);

	// Mouse clicking events
	virtual void OnMouseDown(sf::Vector2f cursorPosition);
	virtual void OnMouseHold(sf::Vector2f cursorPosition);
	virtual void OnMouseRelease(sf::Vector2f cursorPosition);

	// OnEnable is called whenever the object is set to active.
	virtual void OnEnable();
	// OnDisable is called whenever the object is set to inactive.
	virtual void OnDisable();

	// OnDestroy is called whenever the object is destroyed.
	virtual void OnDestroy();

	// This method calculats the bounding box. It is virtual and thus different for each object.
	virtual void CalculateBoundingBox();
	// Calculates the object's bounding box with a given sprite in mind.
	void CalculateBoundingBoxFromSprite(sf::Sprite sprite);

private:
	// Methods
	
	// Members
	bool m_isActive;
	int m_drawOrder;

	// Transform
	sf::Transformable m_transform;

	// Cursor input and positioning
	bool m_cursorIsHoveringObject;
	bool m_cursorIsHoveringObjectPreviousFrame;

	bool m_cursorIsDownOnObject;
	bool m_cursorIsDownOnObjectPreviousFrame;

	// Used for velocity
	sf::Vector2f m_previousPosition;
	sf::Time m_previousDeltaTime;
};


// This struct is used when sorting the GameObjects in a given scene by their draw orders.
struct CompareGameObjectDrawOrder {
	bool operator()(GameObject *lhs, GameObject *rhs) {
		return lhs->GetDrawOrder() < rhs->GetDrawOrder();
	}
};

#endif // __GameObject
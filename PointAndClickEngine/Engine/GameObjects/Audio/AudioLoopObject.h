#ifndef __AudioLoopObjectH
#define __AudioLoopObjectH

#include <Engine/GameObjects/GameObject.h>
#include <SFML/Audio.hpp>

class AudioLoopObject :	public GameObject {

public:
	AudioLoopObject();
	AudioLoopObject(std::string pathToSoundInAssetFolder);
	virtual ~AudioLoopObject();

	void StartLoop();
	void PauseLoop();
	void ResumeLoop();
	void StopLoop();
	void RestartLoop();

protected:
	virtual void OnDestroy() override;

private:
	bool m_isValid;
	sf::Music m_loopSound;
};

#endif // __AudioLoopObjectH
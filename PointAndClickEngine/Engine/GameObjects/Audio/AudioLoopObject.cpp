#include "AudioLoopObject.h"
#include <Engine/File Manager/FileManager.h>

AudioLoopObject::AudioLoopObject()
{
	m_isValid = false;
}

AudioLoopObject::AudioLoopObject(std::string pathToSoundInAssetFolder)
{
	m_isValid = false;

	std::string path = FileManager::GetPathToAsset(pathToSoundInAssetFolder);
	
	if(!m_loopSound.openFromFile(path))
	{
		printf("File at '%s' could not be loaded! AudioLoopObject will not be playing.\n", pathToSoundInAssetFolder.c_str());
		return;
	}

	m_isValid = true;
	StartLoop();
	
}

AudioLoopObject::~AudioLoopObject() {}


void AudioLoopObject::StartLoop()
{
	if (!m_isValid)
		return;

	m_loopSound.setLoop(true);
	m_loopSound.play();
}

void AudioLoopObject::PauseLoop()
{
	if (!m_isValid)
		return;

	m_loopSound.pause();
}

void AudioLoopObject::ResumeLoop() {
	if (!m_isValid)
		return;

	m_loopSound.play();
}

void AudioLoopObject::StopLoop()
{
	if (!m_isValid)
		return;

	m_loopSound.stop();
}

void AudioLoopObject::RestartLoop()
{
	if (!m_isValid)
		return;

	StopLoop();
	StartLoop();
}

void AudioLoopObject::OnDestroy()
{
	StopLoop();
}
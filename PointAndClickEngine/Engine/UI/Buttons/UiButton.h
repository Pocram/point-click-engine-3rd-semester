#ifndef __UiButtonH
#define __UiButtonH

#include "SFML/Graphics.hpp"

class UiButton {

public:
	// Constructors
	UiButton();
	virtual ~UiButton();


	// Methods
	virtual void Update(sf::Time elapsedTime);
	virtual void Draw(sf::RenderWindow *window);

	bool GetMouseEvents(bool someButtonFound);
	bool IsCursorHovering();

	void SetPositionInUiRect(sf::Vector2f position);
	
	sf::Vector2f GetPositionInUiRect();
	sf::Vector2f GetPositionInWindow();

	sf::FloatRect GetBounds();


protected:
	// Methods
	virtual void UpdateBounds();


	// Members
	sf::RectangleShape* m_uiRect;
	sf::FloatRect m_bounds;


private:
	// Methods
	virtual void OnMouseEnter(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseHover(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseExit(sf::Vector2f cursorWorldPosition);	
	virtual void OnMouseDown(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseHold(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseRelease(sf::Vector2f cursorWorldPosition);

	// Members
	sf::Vector2f m_position;

	

	// Mouse Events
	bool m_cursorIsHoveringButtonPreviousFrame;
	bool m_cursorIsHoveringButton;	
	bool m_cursorIsDownOnButtonPreviousFrame;
	bool m_cursorIsDownOnButton;


};

#endif // __UiButtonH
#include "ActionUiButton.h"
#include <Engine/File Manager/FontManager.h>

ActionUiButton::ActionUiButton() {
	m_interactionMode = GameLogic::InteractionMode::None;
}

ActionUiButton::ActionUiButton(std::string text, std::string fontName, unsigned fontSize, sf::Vector2f positionInUiRect, sf::RectangleShape* uiRect, GameLogic::InteractionMode interactionMode)
{
	sf::Font *font = FontManager::GetFont(fontName);
	if (font == NULL) {
		printf("# ERROR: Font '%s' was not found!\n", fontName.c_str());
		return;
	}

	m_uiRect = uiRect;

	m_text.setFont(*font);
	m_text.setCharacterSize(fontSize);
	m_text.setString(text);

	SetPositionInUiRect(positionInUiRect);
	m_text.setPosition(GetPositionInWindow());
	m_bounds = m_text.getGlobalBounds();

	m_text.setColor(sf::Color::Magenta);

	m_interactionMode = interactionMode;
}

ActionUiButton::~ActionUiButton() {}


void ActionUiButton::OnMouseDownEvent(sf::Vector2f cursorWorldPosition)
{
	// Reset mode if clicked twice
	if(GameLogic::GetCurrentInteractionMode() == m_interactionMode)
	{
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
		return;
	}

	GameLogic::SetInteractionMode(m_interactionMode);
}
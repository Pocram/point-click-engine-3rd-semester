#ifndef __ActionUiButtonHead
#define __ActionUiButtonHead

#include <Engine/UI/Buttons/TextUiButton.h>
#include <Engine/Game Logic/GameLogic.h>

class ActionUiButton : public TextUiButton {

public:
	// Constructors
	ActionUiButton();
	ActionUiButton(std::string text, std::string fontName, unsigned fontSize, sf::Vector2f positionInUiRect, sf::RectangleShape* uiRect, GameLogic::InteractionMode interactionMode);
	virtual ~ActionUiButton();

	// Methods
	virtual void OnMouseDownEvent(sf::Vector2f cursorWorldPosition) override;


private:
	// Members
	GameLogic::InteractionMode m_interactionMode;
};

#endif // __ActionUiButtonHead
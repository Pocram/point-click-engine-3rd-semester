#include "UiButton.h"
#include <Engine/UI/GameInterface.h>
#include <Engine/Scenes/Camera/Camera.h>
#include <Engine/Scenes/GameScene.h>

extern GameScene CurrentScene;

UiButton::UiButton()
{
	m_position = sf::Vector2f(0.f, 0.f);
}

UiButton::~UiButton() {}


void UiButton::Update(sf::Time elapsedTime)
{
}

void UiButton::Draw(sf::RenderWindow* window)
{
}


bool UiButton::GetMouseEvents(bool someButtonFound)
{
	bool inputDetected = false;

	m_cursorIsHoveringButtonPreviousFrame = m_cursorIsHoveringButton;
	m_cursorIsDownOnButtonPreviousFrame = m_cursorIsDownOnButton;

	// Cursor checks
	Camera camera = CurrentScene.GetCamera();
	// Get the cursor position relative to the window
	sf::Vector2f cursorPosition = static_cast<sf::Vector2f>(sf::Mouse::getPosition(*GameWindow));

	// Check if cursor is within UI bounds
	if (cursorPosition.x < 0 || cursorPosition.x > GameWindow->getSize().x
		|| cursorPosition.y < GameWindow->getSize().y - GameUI::GetSize().y || cursorPosition.y > GameWindow->getSize().y) {
		// Cursor is outside of window
		m_cursorIsHoveringButton = false;
		m_cursorIsDownOnButton = false;
	} else {
		// Cursor is on window

		if (GetBounds().contains(cursorPosition)) {
			m_cursorIsHoveringButton = true;

			// Get
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				m_cursorIsDownOnButton = true;
			} else {
				m_cursorIsDownOnButton = false;
			}
		} else {
			m_cursorIsHoveringButton = false;
			m_cursorIsDownOnButton = false;
		}
	}


	// Hover enter
	if (!someButtonFound && m_cursorIsHoveringButton && !m_cursorIsHoveringButtonPreviousFrame) {
		OnMouseEnter(cursorPosition);
		inputDetected = true;
	}
	// Hover stay
	else if (!someButtonFound && m_cursorIsHoveringButton && m_cursorIsHoveringButtonPreviousFrame) {
		OnMouseHover(cursorPosition);
		inputDetected = true;
	}
	// Hover exit
	else if (!m_cursorIsHoveringButton && m_cursorIsHoveringButtonPreviousFrame) {
		OnMouseExit(cursorPosition);
	}


	// Click
	if (!someButtonFound && m_cursorIsDownOnButton && !m_cursorIsDownOnButtonPreviousFrame) {
		OnMouseDown(cursorPosition);
		inputDetected = true;
	}
	// Hold
	else if (!someButtonFound && m_cursorIsDownOnButton && m_cursorIsDownOnButtonPreviousFrame) {
		OnMouseHold(cursorPosition);
		inputDetected = true;
	}
	// Release
	else if (!m_cursorIsDownOnButton && m_cursorIsDownOnButtonPreviousFrame) {
		OnMouseRelease(cursorPosition);
	}

	return inputDetected;
}

bool UiButton::IsCursorHovering()
{
	return m_cursorIsHoveringButton;
}

void UiButton::SetPositionInUiRect(sf::Vector2f position)
{
	m_position = position + m_uiRect->getPosition();
	UpdateBounds();
}

sf::Vector2f UiButton::GetPositionInUiRect()
{
	if (m_uiRect == NULL)
		return sf::Vector2f(0.f, 0.f);

	return m_position - m_uiRect->getPosition();
}

sf::Vector2f UiButton::GetPositionInWindow()
{
	return m_position;
}

sf::FloatRect UiButton::GetBounds()
{
	return m_bounds;
}

void UiButton::UpdateBounds()
{
}


void UiButton::OnMouseEnter(sf::Vector2f cursorWorldPosition)
{
}

void UiButton::OnMouseHover(sf::Vector2f cursorWorldPosition)
{
}

void UiButton::OnMouseExit(sf::Vector2f cursorWorldPosition)
{
}

void UiButton::OnMouseDown(sf::Vector2f cursorWorldPosition)
{
}

void UiButton::OnMouseHold(sf::Vector2f cursorWorldPosition)
{
}

void UiButton::OnMouseRelease(sf::Vector2f cursorWorldPosition)
{
}
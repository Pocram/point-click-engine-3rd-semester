#ifndef __UiItemButtonH
#define __UiItemButtonH

#include <SFML/Graphics.hpp>
#include "UiButton.h"

class UiItemButton : public UiButton {

public:
	// Constructors
	UiItemButton();
	UiItemButton(int inventorySlotId, sf::Vector2f positionInUiRect, sf::Vector2f dimensions, sf::RectangleShape* uiRect);
	~UiItemButton();


	// Methods
	virtual void Update(sf::Time elapsedTime);
	virtual void Draw(sf::RenderWindow *window);

	virtual void OnMouseEnter(sf::Vector2f cursorWorldPosition) override;
	virtual void OnMouseHover(sf::Vector2f cursorWorldPosition) override;
	virtual void OnMouseExit(sf::Vector2f cursorWorldPosition) override;
	virtual void OnMouseDown(sf::Vector2f cursorWorldPosition) override;
	virtual void OnMouseHold(sf::Vector2f cursorWorldPosition) override;
	virtual void OnMouseRelease(sf::Vector2f cursorWorldPosition) override;


protected:
	virtual void UpdateBounds() override;


private:
	// Members
	int m_inventorySlotId;
	sf::RectangleShape m_rectangleShape;
	sf::RectangleShape m_backdropShape;

	float m_selectionTime;
};

#endif // __UiItemButtonH
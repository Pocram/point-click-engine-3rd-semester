#include "TextUiButton.h"
#include "Engine/File Manager/FontManager.h"

TextUiButton::TextUiButton()
{
	m_text = sf::Text();
}

TextUiButton::TextUiButton(std::string text, std::string fontName, unsigned fontSize, sf::Vector2f positionInUiRect, sf::RectangleShape* uiRect)
{
		sf::Font *font = FontManager::GetFont(fontName);
		if(font == NULL)
		{
			printf("# ERROR: Font '%s' was not found!\n", fontName.c_str());
			return;
		}

		m_uiRect = uiRect;

		m_text.setFont(*font);
		m_text.setCharacterSize(fontSize);
		m_text.setString(text);
		
		SetPositionInUiRect(positionInUiRect);
		m_text.setPosition(GetPositionInWindow());
		m_bounds = m_text.getGlobalBounds();

		m_text.setColor(sf::Color::Magenta);
}

TextUiButton::~TextUiButton() {}



void TextUiButton::Update(sf::Time elapsedTime)
{
}

void TextUiButton::Draw(sf::RenderWindow* window)
{
	window->draw(m_text);
}

void TextUiButton::SetText(std::string newText)
{
	m_text.setString(newText);
}

void TextUiButton::UpdateBounds()
{
	m_bounds = m_text.getGlobalBounds();
}


void TextUiButton::OnMouseEnter(sf::Vector2f cursorWorldPosition)
{
	m_text.setColor(sf::Color(255, 127, 0));
	OnMouseEnterEvent(cursorWorldPosition);
}

void TextUiButton::OnMouseHover(sf::Vector2f cursorWorldPosition)
{
	OnMouseHoverEvent(cursorWorldPosition);
}

void TextUiButton::OnMouseExit(sf::Vector2f cursorWorldPosition)
{
	m_text.setColor(sf::Color::Magenta);
	OnMouseExitEvent(cursorWorldPosition);
}

void TextUiButton::OnMouseDown(sf::Vector2f cursorWorldPosition)
{
	m_text.setColor(sf::Color::White);
	OnMouseDownEvent(cursorWorldPosition);
}

void TextUiButton::OnMouseHold(sf::Vector2f cursorWorldPosition)
{
	OnMouseHoldEvent(cursorWorldPosition);
}

void TextUiButton::OnMouseRelease(sf::Vector2f cursorWorldPosition)
{
	if(IsCursorHovering())
		m_text.setColor(sf::Color(255, 127, 0));
	else
		m_text.setColor(sf::Color::Magenta);

	OnMouseReleaseEvent(cursorWorldPosition);
}


void TextUiButton::OnMouseEnterEvent(sf::Vector2f cursorWorldPosition)
{
	// Used by child classes
}

void TextUiButton::OnMouseHoverEvent(sf::Vector2f cursorWorldPosition)
{
	// Used by child classes
}

void TextUiButton::OnMouseExitEvent(sf::Vector2f cursorWorldPosition)
{
	// Used by child classes
}

void TextUiButton::OnMouseDownEvent(sf::Vector2f cursorWorldPosition)
{
	// Used by child classes
}

void TextUiButton::OnMouseHoldEvent(sf::Vector2f cursorWorldPosition)
{
	// Used by child classes
}

void TextUiButton::OnMouseReleaseEvent(sf::Vector2f cursorWorldPosition)
{
	// Used by child classes
}
#ifndef __TextUiButtonH
#define __TextUiButtonH

#include <Engine/UI/Buttons/UiButton.h>

class TextUiButton : public UiButton {

public:
	// Constructors
	TextUiButton();
	TextUiButton(std::string text, std::string fontName, unsigned int fontSize, sf::Vector2f positionInUiRect, sf::RectangleShape *uiRect);
	~TextUiButton();


	// Methods
	virtual void Update(sf::Time elapsedTime) override;
	virtual void Draw(sf::RenderWindow *window) override;

	virtual void SetText(std::string newText);


protected:
	virtual void UpdateBounds() override;

	// Members
	sf::Text m_text;


private:
	// Methods
	void OnMouseEnter(sf::Vector2f cursorWorldPosition) override;
	void OnMouseHover(sf::Vector2f cursorWorldPosition) override;
	void OnMouseExit(sf::Vector2f cursorWorldPosition) override;
	void OnMouseDown(sf::Vector2f cursorWorldPosition) override;
	void OnMouseHold(sf::Vector2f cursorWorldPosition) override;
	void OnMouseRelease(sf::Vector2f cursorWorldPosition) override;

	virtual void OnMouseEnterEvent(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseHoverEvent(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseExitEvent(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseDownEvent(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseHoldEvent(sf::Vector2f cursorWorldPosition);
	virtual void OnMouseReleaseEvent(sf::Vector2f cursorWorldPosition);

};

#endif // __TextUiButtonH
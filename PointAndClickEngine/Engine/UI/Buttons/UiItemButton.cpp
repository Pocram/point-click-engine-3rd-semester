#include <math.h>

#include "UiItemButton.h"
#include <Engine/Items/Inventory.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/File Manager/TextureManager.h>
#include <Engine/Scenes/Camera/Camera.h>

UiItemButton::UiItemButton()
{
	m_inventorySlotId = -1;
	m_selectionTime = 0.0f;
}

UiItemButton::UiItemButton(int inventorySlotId, sf::Vector2f positionInUiRect, sf::Vector2f dimensions, sf::RectangleShape* uiRect)
{
	m_selectionTime = 0.0f;

	m_inventorySlotId = inventorySlotId;

	m_rectangleShape = sf::RectangleShape(dimensions);
	m_backdropShape = sf::RectangleShape(dimensions);
		
	// The outline is used for selecting items in the inventory bar
	m_rectangleShape.setOutlineColor(sf::Color::Yellow);
	m_rectangleShape.setOutlineThickness(0.0f);

	m_uiRect = uiRect;
	SetPositionInUiRect(positionInUiRect);
	m_rectangleShape.setPosition(GetPositionInWindow());
	m_backdropShape.setPosition(GetPositionInWindow());

	m_backdropShape.setTexture(TextureManager::GetTexture("Sprites/Items/ItemBackdrop"));

	UpdateBounds();
}

UiItemButton::~UiItemButton() {}


void UiItemButton::Update(sf::Time elapsedTime)
{
	Item* correspondingItem = Inventory::GetItemInSlot(m_inventorySlotId);

	// Set texture depending on item slot
	if(correspondingItem == NULL || !correspondingItem->m_IsValid)
	{
		m_rectangleShape.setFillColor(sf::Color(255, 255, 255, 0));
	} 
	else
	{
		m_rectangleShape.setFillColor(sf::Color(255, 255, 255, 255));
		m_rectangleShape.setTexture(correspondingItem->GetTexture());
	}

	// Set color depending on selection
	if(correspondingItem != NULL && Inventory::GetCurrentlySelectedItem() == correspondingItem)
	{
		m_selectionTime += elapsedTime.asSeconds();
		m_rectangleShape.setOutlineThickness(2.0f);
		
		// Do a cool fade animation!
		sf::Color color = m_rectangleShape.getOutlineColor();
		color.a = (0.25f * sin(m_selectionTime * 4.0f) + 0.5f) * 255;
		m_rectangleShape.setOutlineColor(color);

		// The wavelength of a sine wave is pie. To prevent an eventual buffer overflow, just reset it at pie.
		if(m_selectionTime >= 3.14f)
		{
			m_selectionTime = 0.0f;
		}
	} 
	else
	{
		m_rectangleShape.setOutlineThickness(0.0f);
	}
}

void UiItemButton::Draw(sf::RenderWindow* window)
{
	window->draw(m_backdropShape);
	window->draw(m_rectangleShape);
}


void UiItemButton::OnMouseEnter(sf::Vector2f cursorWorldPosition)
{
	Inventory::HoverItem(m_inventorySlotId);
}

void UiItemButton::OnMouseHover(sf::Vector2f cursorWorldPosition)
{
}

void UiItemButton::OnMouseExit(sf::Vector2f cursorWorldPosition)
{
	Inventory::UnHoverItem();
}

void UiItemButton::OnMouseDown(sf::Vector2f cursorWorldPosition)
{
	Item* item = Inventory::GetItemInSlot(m_inventorySlotId);
	// Don't do anything if no item is in the slot. Otherwise an exception will be thrown.
	if(item == NULL)
	{
		return;
	}

	printf("Clicked on inventory slot %d. Item: '%d, %s'\n", m_inventorySlotId, Inventory::GetItemInSlot(m_inventorySlotId)->m_ID, Inventory::GetItemInSlot(m_inventorySlotId)->m_Name.c_str());
	
	m_selectionTime = 0.0f;

	if(GameLogic::GetCurrentInteractionMode() != GameLogic::InteractionMode::None)
	{
		// 'Look at' interaction
		if(GameLogic::GetCurrentInteractionMode() == GameLogic::InteractionMode::LookAt)
		{
			// Display message on screen
			sf::Vector2f position = CurrentScene.GetCamera().ScreenToWindowPoint({ 0.5f, 0.95f });
			CurrentScene.ShowTextOnScreen(item->GetLookAtText(), position, 4.0f);
		}
		
		// Reset mode
		GameLogic::SetInteractionMode(GameLogic::InteractionMode::None);
	} 
	else
	{
		// Only select the item if no interaction mode is active
		// Unselect the current item if clicked twice
		if(Inventory::GetCurrentlySelectedItem() == item)
		{
			Inventory::UnselectItem();
		} 
		else
		{
			Inventory::SelectItemInSlot(m_inventorySlotId);
		}
	}
}

void UiItemButton::OnMouseHold(sf::Vector2f cursorWorldPosition)
{
}

void UiItemButton::OnMouseRelease(sf::Vector2f cursorWorldPosition)
{
}

void UiItemButton::UpdateBounds()
{
	m_bounds = m_rectangleShape.getGlobalBounds();
}
#include "UiMessage.h"
#include <Engine/File Manager/FontManager.h>

UiMessage::UiMessage()
{
	m_message = "";
	m_position = sf::Vector2f(0.0f, 0.0f);
	m_duration = 0.0f;

	m_textComponent = sf::Text();
}

UiMessage::UiMessage(std::string message, sf::Vector2f position, float duration)
{
	m_message = message;
	m_position = position;
	m_duration = duration;

	sf::Font *font = FontManager::GetFont("PostinoStd");
	if (font == NULL)
	{
		m_textComponent = sf::Text();
		return;
	}

	m_textComponent = sf::Text(message, *font, 15);
	
	sf::FloatRect tBounds = m_textComponent.getGlobalBounds();
	m_textComponent.setOrigin(tBounds.width / 2.0f, tBounds.height / 2.0f);
	m_textComponent.setPosition(position);
}

UiMessage::~UiMessage() {}

std::string UiMessage::GetMessage()
{
	return m_message;
}

sf::Vector2f UiMessage::GetPosition()
{
	return m_position;
}

float UiMessage::GetDuration()
{
	return m_duration;
}

sf::Text UiMessage::GetTextComponent()
{
	return m_textComponent;
}

void UiMessage::RemoveFromDuration(float delta)
{
	m_duration -= delta;
}
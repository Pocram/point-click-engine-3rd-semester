#ifndef __UiMessageH
#define __UiMessageH

#include <string>
#include <SFML/Graphics.hpp>

class UiMessage {

public:
	// Constructors
	UiMessage();
	UiMessage(std::string message, sf::Vector2f position, float duration);
	~UiMessage();

	// Getters
	std::string GetMessage();
	sf::Vector2f GetPosition();
	float GetDuration();

	sf::Text GetTextComponent();

	void RemoveFromDuration(float delta);


private:
	std::string m_message;
	sf::Vector2f m_position;
	float m_duration;

	sf::Text m_textComponent;
};

#endif // UiMessageH
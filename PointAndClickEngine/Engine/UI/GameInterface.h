#ifndef __GameInterfaceH
#define __GameInterfaceH

#include "SFML/Graphics.hpp"
#include <Engine/GameObjects/GameObject.h>
#include <Engine/UI/Buttons/UiButton.h>

namespace GameUI
{
	
	void Init();

	void Update(sf::Time elapsedTime);

	void Draw(sf::RenderWindow *window);

	
	sf::Vector2f GetSize();

	GameObject* FindGameObject(std::string name);

}

namespace GameUIInternal
{
	void SetUiCommandText();
}

#endif // __GameInterfaceH
#include "GameInterface.h"
#include <Engine/Scenes/GameScene.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Items/Inventory.h>

#include <Engine/File Manager/FontManager.h>

// Buttons
#include <Engine/UI/Buttons/TextUiButton.h>
#include "Buttons/ActionButtons/ActionUiButton.h"
#include "Buttons/UiItemButton.h"

#include "boost/foreach.hpp"
#include <Engine/GameObjects/Interactables/InteractableObject.h>


extern sf::RenderWindow* GameWindow;
extern GameScene CurrentScene;

// Variables
// ======================================================
// The black rectangle at the bottom of the screen
sf::RectangleShape UiRect;

// Buttons
// ----------------------------
ActionUiButton LookAtButton;
ActionUiButton UseButton;

// Inventory
// ----------------------------
const int _ItemsPerRow = 5;
const float _InventoryXPosition = 608.0f;
const float _InventoryItemDimensions = 50.f;
const float _InventoryItemPadding = 6.0f;
UiItemButton ItemButtons[10];

// UI command text
// ----------------------------
// Position is relative to position in UI rect
sf::Vector2f _UiCommandPosition = sf::Vector2f(0.675f, 0.075f);
sf::Text UiCommandText;


void GameUI::Init()
{
	printf("-- Initialising UI...\n");

	// Set size and position of the rectangle
	UiRect.setSize(sf::Vector2f(GameWindow->getSize().x, 120.f));
	UiRect.setPosition(0.f, GameWindow->getSize().y - UiRect.getSize().y);
	UiRect.setFillColor(sf::Color::Black);

	// Buttons
	// ----------------------------
	LookAtButton = ActionUiButton("Look at", "PostinoStd", 20, sf::Vector2f(5.f, 5.f), &UiRect, GameLogic::InteractionMode::LookAt);
	UseButton = ActionUiButton("Use", "PostinoStd", 20, sf::Vector2f(5.f, 30.f), &UiRect, GameLogic::InteractionMode::Use);


	// Inventory
	// ----------------------------
	for (int i = 0; i < 10; i++) {
		// Calculate position of button
		float xPos = _InventoryXPosition + (i % _ItemsPerRow) * (_InventoryItemDimensions + _InventoryItemPadding);
		float currentRow = i > 4 ? 1.0f : 0.0f;
		float yPos = _InventoryItemPadding + currentRow * (_InventoryItemDimensions + _InventoryItemPadding);

		ItemButtons[i] = UiItemButton(i, sf::Vector2f(xPos, yPos), sf::Vector2f(_InventoryItemDimensions, _InventoryItemDimensions), &UiRect);
	}


	// UI command text
	// ----------------------------
	// Calculate screen position
	_UiCommandPosition.x = _UiCommandPosition.x * UiRect.getSize().x + UiRect.getPosition().x;
	_UiCommandPosition.y = _UiCommandPosition.y * UiRect.getSize().y + UiRect.getPosition().y;
	
	UiCommandText = sf::Text();
	UiCommandText.setFont(*FontManager::GetFont("PostinoStd"));
	UiCommandText.setString("");
	UiCommandText.setPosition(_UiCommandPosition);
	UiCommandText.setColor(sf::Color(255, 75, 171));	// #FF4BAB, the official Pigbin c(oo)l(oo)r
	UiCommandText.setCharacterSize(14);

	printf("-- Finished initialising UI!\n");
}


void GameUI::Update(sf::Time elapsedTime)
{
	// Handle input on UI elements.
	//Only allow input once per frame to prevent clicking through buttons.
	bool buttonInputWasHandled = false;
	buttonInputWasHandled = LookAtButton.GetMouseEvents(buttonInputWasHandled);
	buttonInputWasHandled = UseButton.GetMouseEvents(buttonInputWasHandled);
	for (int i = 0; i < 10; i++) {
		buttonInputWasHandled = ItemButtons[i].GetMouseEvents(buttonInputWasHandled);
	}
	
	// Update elements
	LookAtButton.Update(elapsedTime);
	UseButton.Update(elapsedTime);
	for (int i = 0; i < 10; i++) {
		ItemButtons[i].Update(elapsedTime);
	}


	// Command text
	// -------------------
	// Update text
	GameUIInternal::SetUiCommandText();

	// Align
	sf::FloatRect tBounds = UiCommandText.getGlobalBounds();
	UiCommandText.setOrigin(tBounds.width, tBounds.height / 2.0f);
}

void GameUI::Draw(sf::RenderWindow* window)
{
	window->draw(UiRect);
	LookAtButton.Draw(window);
	UseButton.Draw(window);

	for (int i = 0; i < 10; i++) {
		ItemButtons[i].Draw(window);
	}

	window->draw(UiCommandText);
}


sf::Vector2f GameUI::GetSize()
{
	return UiRect.getSize();
}


GameObject* GameUI::FindGameObject(std::string name)
{
	std::list<GameObject*> objectsInScene = CurrentScene.GetGameObjects();
	BOOST_FOREACH(GameObject* go, objectsInScene) {
		if (go->m_Name == name) {
			return go;
		}
	}

	return NULL;
}


void GameUIInternal::SetUiCommandText()
{
	// DISPLAY HIERARCHY
	/*
	Use
	Use objHover
	Use itemHover
	Use item with
	Use item with itemHover
	Use item with objHover

	objHover
	itemHover
	item
	*/


	// Get the currently selected objects and states
	GameLogic::InteractionMode interactionMode = GameLogic::GetCurrentInteractionMode();
	std::string interactionModeStr = GameLogic::GetInteractionModeString(interactionMode);
	
	Item* selectedItem = Inventory::GetCurrentlySelectedItem();
	Item* hoveredItem = Inventory::GetCurrentlyHoveredItem();

	GameObject* selectedObject = InteractableObject::GetCurrentlySelectedObject();

	std::string newText = interactionModeStr;

	// Commands without interaction modes
	if(interactionMode == GameLogic::InteractionMode::None)
	{
		if(selectedObject != NULL)
		{
			UiCommandText.setString(selectedObject->m_Name);
			return;
		}

		if(hoveredItem != NULL)
		{
			UiCommandText.setString(hoveredItem->m_Name);
			return;
		}

		if(selectedItem != NULL)
		{
			UiCommandText.setString(selectedItem->m_Name);
			return;
		}

		UiCommandText.setString("");
		return;
	} 
	else
	{
		// Interaction mode only
		if(selectedItem == NULL && selectedObject == NULL && hoveredItem == NULL)
		{
			UiCommandText.setString(interactionModeStr + "...");
			return;
		}

		// Object only
		if(selectedObject != NULL && selectedItem == NULL)
		{
			UiCommandText.setString(interactionModeStr + " " + selectedObject->m_Name);
			return;
		}

		// Item hover only
		if (selectedItem == NULL && hoveredItem != NULL) {
			UiCommandText.setString(interactionModeStr + " " + hoveredItem->m_Name);
			return;
		}

		// Item only
		if (selectedObject == NULL && selectedItem != NULL) {
			UiCommandText.setString(interactionModeStr + " " + selectedItem->m_Name + " with...");
			return;
		}

		// Item and item
		if (selectedItem != NULL && hoveredItem != NULL) {
			UiCommandText.setString(interactionModeStr + " " + selectedItem->m_Name + " with " + hoveredItem->m_Name);
			return;
		}

		// Item and object
		if (selectedItem != NULL && selectedObject != NULL) {
			UiCommandText.setString(interactionModeStr + " " + selectedItem->m_Name + " with " + selectedObject->m_Name);
			return;
		}
	}
}
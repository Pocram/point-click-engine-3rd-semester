#include "windows.h"

#include "sstream"

#include "GameLoop.h"
#include <Engine/UI/GameInterface.h>
#include <Engine/Game Logic/GameLogic.h>
#include <Engine/Items/Inventory.h>
#include "GameObjects/Interactables/InteractableObject.h"

#include <Game/Scenes/01_Forest/Scene01_Forest.h>
#include <Game/Scenes/02_Fortress/Scene02_Fortress.h>
#include <Game/Scenes/03_DimensionHole/Scene03_DimensionHole.h>

bool GAME_IS_RUNNING = true;

GameScene CurrentScene;
GameScene _NextScene;

sf::RenderWindow *GameWindow;

void StartGameLoop(sf::RenderWindow * window)
{
	GameWindow = window;

	LoadScene(Scene01_Forest());
	//LoadScene(Scene02_Fortress());
	//LoadScene(Scene03_DimensionHole());
	_LoadSceneInternal();

	sf::Clock gameLoopClock;
	sf::Clock renderClock;


	// Set the target frame rate and cap the render speed accordingly.
	int targetFrameRate = 60;
	window->setFramerateLimit(targetFrameRate);
	window->setVerticalSyncEnabled(true);

	GameLogic::Init();
	GameUI::Init();
	Inventory::InitInventory();

	printf("GAME LOOP STARTED\n-------------------------------------------------------------------------\n");

	while (window->isOpen()) {
		sf::Event event;
		while (window->pollEvent(event) && GAME_IS_RUNNING) {
			// Close the window when ESC is pressed or X is clicked.
			if (event.type == sf::Event::Closed
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				FreeConsole();
				window->close();
				GAME_IS_RUNNING = false;
			}
		}

		// LOOP COURTESY OF https://riseagain.wordpress.com/2012/07/19/sfml-2-tutorial-break-out/
		// No, I do NOT know what c and t are for, their code is just _great_.

		// Get render time and FPS
		float currentFps = 1000000 / renderClock.getElapsedTime().asMicroseconds();
		std::stringstream newTitle;
		newTitle << "Pointless Click Engine (" << currentFps << " fps)";
		window->setTitle(newTitle.str());

		renderClock.restart();

		// Do something
		const sf::Int64 frameTime = 1000000 / targetFrameRate;
		sf::Clock c;
		sf::Time t = c.getElapsedTime();
		sf::Int64 nextFrameTime = t.asMicroseconds() + frameTime;

		// Update loop
		int loops = 0;
		while( t.asMicroseconds() < nextFrameTime && loops < 5)
		{
			Update(gameLoopClock.restart());
			t = c.getElapsedTime();
			loops++;
		}

		// Actually render now
		Draw(window);

		// If a new scene shall be loaded, load it.
		// Scenes are loaded at the end of the game loop. This prevents the loop from accessing data that is no longer there and crashing the game.
		_LoadSceneInternal();
	}
}


void Update(sf::Time elapsedTime)
{
	GameUI::Update(elapsedTime);
	CurrentScene.Update(elapsedTime);
}

void Draw(sf::RenderWindow *window)
{
	window->clear();
	CurrentScene.Draw(window);
	GameUI::Draw(window);
	window->display();
}


void LoadScene(GameScene scene)
{
	_NextScene = scene;
}

void _LoadSceneInternal()
{
	if (_NextScene.GetGameObjects() == CurrentScene.GetGameObjects())
		return;

	// Remove any selections to prevent crashes and error
	InteractableObject::SetCurrentlySelectedObject(NULL);
	Inventory::UnHoverItem();
	Inventory::UnselectItem();

	CurrentScene.Unload();
	printf("Loading scene '%s'...\n", _NextScene.GetName().c_str());
	CurrentScene = _NextScene;
	_NextScene = CurrentScene;
}
#ifndef __InteractionModeH
#define __InteractionModeH

#include <string>

#include <Engine/Scenes/GameScene.h>

namespace GameLogic
{
	// Types
	enum InteractionMode {
		None,
		LookAt,
		Use,
		PickUp
	};

	// Methods
	void Init();

	std::string GetInteractionModeString(InteractionMode mode);

	InteractionMode GetCurrentInteractionMode();
	void SetInteractionMode(InteractionMode mode);

	GameScene* GetCurrentScene();
}

#endif // __InteractionModeH
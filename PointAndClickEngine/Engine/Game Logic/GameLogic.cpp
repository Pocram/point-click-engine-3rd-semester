#include "GameLogic.h"

extern GameScene CurrentScene;

// Variables
GameLogic::InteractionMode CurrentInteractionMode;


void GameLogic::Init()
{
	printf("-- Initialising game logic...\n");

	SetInteractionMode(static_cast<InteractionMode>(0));

	printf("-- Finished initialising game logic!\n");
}

std::string GameLogic::GetInteractionModeString(InteractionMode mode)
{
	if (mode < 0)
		mode = InteractionMode::LookAt;

	std::string stringArray[] = {
		"",
		"Look at",
		"Use",
		"Pick up"
	};

	return stringArray[mode];
}

GameLogic::InteractionMode GameLogic::GetCurrentInteractionMode()
{
	return CurrentInteractionMode;
}

void GameLogic::SetInteractionMode(InteractionMode mode)
{
	if (mode < 0)
		mode = static_cast<InteractionMode>(0);

	if (mode > 2)
		mode = static_cast<InteractionMode>(3);

	printf("Setting current InteractionMode to '%s' (%d)\n", GetInteractionModeString(mode).c_str(), mode);
	CurrentInteractionMode = mode;
}

GameScene* GameLogic::GetCurrentScene()
{
	return &CurrentScene;
}
#ifndef __ItemManagerH
#define __ItemManagerH

#include <Engine/Items/Item.h>
#include <stdio.h>
#include <map>

namespace ItemManager
{
	void InitItemManager();

	// Finds the item in the map and returns the pointer to it
	Item* GetItem(int itemId);

	Item ParseToItem(std::string pathToXml);
}

#endif // __ItemManagerH
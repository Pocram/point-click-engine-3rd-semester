#ifndef __fileManager
#define __fileManager

#include "boost/filesystem.hpp"
#include <vector>

namespace FileManager
{
	// Methods
	// Gets the executable's directory from the given argv of main().
	void GetApplicationDirectoryFromArgv(char* argv[]);

	// Returns the executable's directory.
	std::string GetApplicationDirectory();

	// Returns the asset folder's directory.
	std::string GetAssetDirectory();
	// Gets path to an object in the asset folder.
	std::string GetPathToAsset(char* pathInAssetFolder);
	std::string GetPathToAsset(std::string pathInAssetFolder);

	// Returns relative path from Assets folder if path is inside it. Otherwise return path.
	std::string GetRelativeAssetPathFromFullPath(boost::filesystem::path pathToAsset);
	std::string GetRelativeAssetPathFromFullPath(std::string pathToAsset);

	bool IsPathPartOfAssetFolder(boost::filesystem::path pathToAsset);

	// Gets all files with a specific file extension in a folder
	std::vector<std::string> GetAllFilesWithExtension(std::string pathToDirectory, std::string fileExtension);
	// Gets all files with a specific extension in a folder and its subfolders
	std::vector<std::string> GetAllFilesWithExtensionWithSubdirectories(std::string pathToDirectory, std::string fileExtension);
}

#endif // __fileManager
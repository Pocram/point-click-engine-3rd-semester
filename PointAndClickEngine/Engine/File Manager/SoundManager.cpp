#include "SoundManager.h"
#include "FileManager.h"

#include "boost/foreach.hpp"

namespace fs = boost::filesystem;

// The loaded sounds
std::map<std::string, sf::SoundBuffer> LoadedSounds;


void SoundManager::InitSoundManager()
{
	printf("-- Loading sounds...\n");
	int soundCount = 0;

	// Get path to the SFX folder.
	std::string pathToFontFolder = FileManager::GetAssetDirectory();

	// Find all MP3s, WAVs and OGGs
	std::vector<std::string> allSoundsInFolder;
	std::vector<std::string> mp3sInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToFontFolder, ".mp3");
	allSoundsInFolder.insert(allSoundsInFolder.end(), mp3sInFolder.begin(), mp3sInFolder.end());
	
	std::vector<std::string> wavsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToFontFolder, ".wav");
	allSoundsInFolder.insert(allSoundsInFolder.end(), wavsInFolder.begin(), wavsInFolder.end());
	
	std::vector<std::string> oggsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToFontFolder, ".ogg");
	allSoundsInFolder.insert(allSoundsInFolder.end(), oggsInFolder.begin(), oggsInFolder.end());

	// Load sounds
	BOOST_FOREACH(std::string soundPath, allSoundsInFolder) {
		sf::SoundBuffer newSound;
		if (!newSound.loadFromFile(soundPath)) {
			// Sound could not be found or loaded, go to the next one
			continue;
		}

		fs::path boostFontPath(soundPath);
		
		std::string fileName = FileManager::GetRelativeAssetPathFromFullPath(soundPath);
		//std::string fileName = boostFontPath.filename().stem().string();

		// Add sound to map
		LoadedSounds.insert_or_assign(fileName, newSound);
		printf("Loaded sound '%s'\n", fileName.c_str());

		soundCount++;
	}

	printf("-- Finished loading sounds: %d of %d sounds loaded!\n", soundCount, allSoundsInFolder.size());
}

sf::SoundBuffer* SoundManager::GetSoundBuffer(std::string soundName)
{
	std::map<std::string, sf::SoundBuffer>::iterator i = LoadedSounds.find(soundName);
	if (i != LoadedSounds.end()) {
		return &i->second;
	}

	return NULL;
}
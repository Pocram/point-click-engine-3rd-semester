#include "ItemManager.h"
#include "FileManager.h"
#include "TextureManager.h"

#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"
#include "Util/tinyxml2.h"

namespace fs = boost::filesystem;
namespace tx = tinyxml2;

std::map<int, Item> LoadedItems;


void ItemManager::InitItemManager() {
	printf("-- Loading items...\n");
	int itemCount = 0;

	// Get path to the items folder.
	std::string pathToItemFolder = FileManager::GetAssetDirectory().append("Items\\");

	// I am using the extension ".gameitem" to prevent complicated verification if the xml file is an item or not.
	std::vector<std::string> allItemsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToItemFolder, ".gameitem");

	// Load items
	BOOST_FOREACH(std::string itemPath, allItemsInFolder) {
		fs::path boostFontPath(itemPath);
		
		Item newItem = ParseToItem(itemPath);
		if(!newItem.m_IsValid)
		{
			continue;
		}

		// Add item to map
		LoadedItems.insert_or_assign(newItem.m_ID, newItem);
		printf("Loaded item %d: '%s'\n", newItem.m_ID, newItem.m_Name.c_str());

		itemCount++;
	}

	printf("-- Finished loading items: %d of %d items loaded!\n", itemCount, allItemsInFolder.size());
}

Item* ItemManager::GetItem(int itemId)
{
	std::map<int, Item>::iterator i = LoadedItems.find(itemId);
	if (i != LoadedItems.end()) {
		return &i->second;
	}

	return NULL;
}

Item ItemManager::ParseToItem(std::string pathToXml)
{
	Item outItem;
	
	// Check if the file exists.
	// If not return an empty, invalid item.
	if(!fs::exists(pathToXml))
	{
		return outItem;
	}

	// Load the item document
	tx::XMLDocument itemDoc;
	itemDoc.LoadFile(pathToXml.c_str());

	// Check if the root node "Item" is there.
	// You know what's cool? That the file extension does not matter, it will still register as an xml file!
	// Just wanted to let that out.
	tx::XMLNode *rootNode = itemDoc.FirstChildElement("Item");
	if(rootNode == NULL)
	{
		printf("No root node 'Item' found in item '%s'!\n", FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
		return outItem;
	}

	// Okay, the document is valid this far. Time to extract the data!
	// ID
	// ------------------------------
	tx::XMLElement *idElement = rootNode->FirstChildElement("ID");
	if (idElement == NULL) {
		printf("No element 'ID' found in item '%s'!\n", FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
		return outItem;
	}
	// Check if the ID is a valid number
	try
	{
		outItem.m_ID = boost::lexical_cast<int>(idElement->GetText());
	} 
	catch(boost::bad_lexical_cast)
	{
		printf("Invalid element 'ID' of item '%s'!\n", FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
		return outItem;
	}


	// NAME
	// ------------------------------
	tx::XMLElement *nameElement = rootNode->FirstChildElement("Name");
	if(nameElement == NULL)
	{
		printf("No element 'Name' found in item '%s'!\n", FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
		return outItem;
	}
	outItem.m_Name = nameElement->GetText();


	// LOOK AT DESCRIPTION
	// ------------------------------
	tx::XMLElement *lookAtTextElement = rootNode->FirstChildElement("LookAtText");
	if (lookAtTextElement == NULL) {
		printf("No element 'LookAtText' found in item '%s'!\n", FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
		return outItem;
	}
	outItem.SetLookAtText(lookAtTextElement->GetText());


	// SPRITE
	// ------------------------------
	tx::XMLElement *spriteElement = rootNode->FirstChildElement("ImageSrc");
	if (spriteElement == NULL) {
		printf("No element 'ImageSrc' found in item '%s'!\n", FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
		return outItem;
	}
	sf::Texture *newTexture = TextureManager::GetTexture(spriteElement->GetText());
	if(newTexture == NULL)
	{
		printf("Texture '%s' could not be found by item '%s'!\n", spriteElement->GetText(), FileManager::GetRelativeAssetPathFromFullPath(pathToXml).c_str());
	} 
	else
	{
		outItem.SetTexture(newTexture);
	}


	// Mark the item as valid.
	outItem.m_IsValid = true;
	return outItem;
}
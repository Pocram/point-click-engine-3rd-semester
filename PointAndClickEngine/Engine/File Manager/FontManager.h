#ifndef  __FontManagerH
#define __FontManagerH

#include <stdio.h>
#include <map>

#include "SFML/Graphics/Font.hpp"


namespace FontManager
{
	void InitFontManager();

	// Finds the font in the map and returns the pointer to it
	sf::Font* GetFont(std::string fontName);
}

#endif // __FontManagerH
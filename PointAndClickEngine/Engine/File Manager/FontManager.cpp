#include "FontManager.h"
#include "FileManager.h"

#include "boost/foreach.hpp"

namespace fs = boost::filesystem;

// A map (or dictionary, for those of us who prefer C#, so pretty much everyone) of the loaded fonts
std::map<std::string, sf::Font> LoadedFonts;


void FontManager::InitFontManager()
{
	printf("-- Loading fonts...\n");
	int fontCount = 0;

	// Get path to the fonts folder. Gosh, this is beautiful.
	std::string pathToFontFolder = FileManager::GetAssetDirectory();
	
	// Find all ttf and otf fonts
	std::vector<std::string> trueTypeFontsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToFontFolder, ".ttf");
	std::vector<std::string> openTypeFontsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToFontFolder, ".otf");
	
	// Well, this is hacky.
	// Anyway, combine the two into one super vector
	std::vector<std::string> allFontsInFolder = trueTypeFontsInFolder;
	allFontsInFolder.insert(allFontsInFolder.end(), openTypeFontsInFolder.begin(), openTypeFontsInFolder.end());


	// Load fonts
	// GOOD GOD BOOST IS AMAZING.
	// LOOK AT THIS! IT'S GOT A FOREACH LOOP!
	// I kind of like C++ now. KIND OF.
	BOOST_FOREACH(std::string fontPath, allFontsInFolder)
	{
		sf::Font newFont;
		if(!newFont.loadFromFile(fontPath))
		{
			// Font could not be found or loaded, go to the next one!
			// Wait what, no printf here?
			// Why, good observation! But no, SFML sends an error message to the console already!
			continue;
		}

		fs::path boostFontPath(fontPath);
		std::string fileName = boostFontPath.filename().stem().string();

		// Add font to map
		LoadedFonts.insert_or_assign(fileName, newFont);
		printf("Loaded font '%s'\n", fileName.c_str());
		
		fontCount++;
	}
	
	printf("-- Finished loading fonts: %d of %d fonts loaded!\n", fontCount, allFontsInFolder.size() );
}

sf::Font* FontManager::GetFont(std::string fontName)
{
	std::map<std::string, sf::Font>::iterator i = LoadedFonts.find(fontName);
	if(i != LoadedFonts.end())
	{
		return &i->second;
	}
	
	return NULL;
}
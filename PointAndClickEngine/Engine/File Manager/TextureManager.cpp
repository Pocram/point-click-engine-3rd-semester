#include "TextureManager.h"
#include "FileManager.h"

#include "boost/foreach.hpp"

namespace fs = boost::filesystem;

// The loaded textures
std::map<std::string, sf::Texture> LoadedTextures;
std::map<std::string, std::size_t> LoadedTextureSizes;


void TextureManager::InitTextureManager()
{
	printf("-- Loading textures...\n");
	int TextureCount = 0;

	// Get path to asset folder.
	std::string pathToAssetFolder = FileManager::GetAssetDirectory();

	// Find all PNGs, JPGs and BMPs
	std::vector<std::string> allTextures;
	std::vector<std::string> pngsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToAssetFolder, ".png");
	allTextures.insert(allTextures.end(), pngsInFolder.begin(), pngsInFolder.end());

	std::vector<std::string> jpgsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToAssetFolder, ".jpg");
	allTextures.insert(allTextures.end(), jpgsInFolder.begin(), jpgsInFolder.end());

	std::vector<std::string> bmpsInFolder = FileManager::GetAllFilesWithExtensionWithSubdirectories(pathToAssetFolder, ".bmp");
	allTextures.insert(allTextures.end(), bmpsInFolder.begin(), bmpsInFolder.end());

	// Load sounds
	BOOST_FOREACH(std::string texturePath, allTextures) {
		sf::Texture newTexture;
		if (!newTexture.loadFromFile(texturePath)) {
			// Texture could not be found or loaded, go to the next one
			continue;
		}

		std::string fileName = FileManager::GetRelativeAssetPathFromFullPath(texturePath);

		// Add texture to map
		LoadedTextures.insert_or_assign(fileName, newTexture);

		// Get size
		FILE *textFile = fopen(texturePath.c_str(), "rb");
		fseek(textFile, 0, SEEK_END);
		std::size_t size = ftell(textFile);

		LoadedTextureSizes.insert_or_assign(fileName, size);
		printf("Loaded texture '%s'\n", fileName.c_str());

		TextureCount++;
	}

	printf("-- Finished loading textures: %d of %d textures loaded!\n", TextureCount, allTextures.size());
}

sf::Texture* TextureManager::GetTexture(std::string textureName)
{
	std::map<std::string, sf::Texture>::iterator i = LoadedTextures.find(textureName);
	if (i != LoadedTextures.end()) {
		return &i->second;
	}

	return NULL;
}

std::size_t TextureManager::GetSizeOfTexture(std::string textureName)
{
	std::map<std::string, std::size_t>::iterator i = LoadedTextureSizes.find(textureName);
	if (i != LoadedTextureSizes.end()) {
		return i->second;
	}

	return NULL;
}
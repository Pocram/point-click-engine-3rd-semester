#ifndef __SoundManagerH
#define __SoundManagerH

#include <stdio.h>
#include <map>

#include <SFML/Audio.hpp>


namespace SoundManager
{
	void InitSoundManager();

	// Finds the sound in the map and returns a pointer to it
	sf::SoundBuffer* GetSoundBuffer(std::string soundName);
}

#endif // __SoundManagerH
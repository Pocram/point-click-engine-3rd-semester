#ifndef __TextureManagerH
#define __TextureManagerH

#include <stdio.h>
#include <map>

#include "SFML/Graphics.hpp"


namespace TextureManager
{
	void InitTextureManager();

	sf::Texture* GetTexture(std::string textureName);
	std::size_t GetSizeOfTexture(std::string textureName);
}

#endif // __TexttureManagerH
#include "FileManager.h"

#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"

#include "Util/tinyxml2.h"

namespace fs = boost::filesystem;
namespace tx = tinyxml2;


// Private methods
fs::path GetPathToMetaFile(fs::path originalPath);
bool MetaFileExists(fs::path originalFilePath, bool createMissingFile);
bool CreateMetaFile(fs::path filePath);

// Returns true or false, depending on the content of the xml file. True means the file will be skipped when loading it into the buffer.
bool CheckIgnorePreload(fs::path metaFilePath);


// Fields
std::string ApplicationDirectory = "";


void FileManager::GetApplicationDirectoryFromArgv(char* argv[])
{
	// Finds the last use of a slash or backslash and saves the content up to that point in a new string.
	std::string path(*argv);
	std::size_t lastSlash = path.find_last_of("/\\");
	std::string directory = path.substr(0, lastSlash);
	ApplicationDirectory = directory;
}

std::string FileManager::GetApplicationDirectory()
{
	return ApplicationDirectory;
}


std::string FileManager::GetAssetDirectory()
{
	// Adds the asset folder to the end of the app directory and returns it.
	std::string exePath = GetApplicationDirectory();
	exePath.append("\\Assets\\");
	return exePath;
}

// Returns the full path to an object inside the asset folder
std::string FileManager::GetPathToAsset(char* pathInAssetFolder)
{
	// Adds the path to the object inside the asset folder to the path of the asset folder and returns it.
	std::string assetDir = GetAssetDirectory();
	
	std::string newPath = assetDir;
	newPath.append(pathInAssetFolder);

	return newPath;
}

// Returns the full path to an object inside the asset folder
std::string FileManager::GetPathToAsset(std::string pathInAssetFolder) {
	// Adds the path to the object inside the asset folder to the path of the asset folder and returns it.
	std::string assetDir = GetAssetDirectory();

	std::string newPath = assetDir;
	newPath.append(pathInAssetFolder);

	return newPath;
}


std::string FileManager::GetRelativeAssetPathFromFullPath(fs::path pathToAsset)
{
	// Return original path if it does not exist or does not point to a file
	if (!fs::exists(pathToAsset) || !fs::is_regular_file(pathToAsset))
		return pathToAsset.string();

	// Check if pathToAsset leads to a path inside the asset folder
	fs::path pathToAssetFolder(GetAssetDirectory());
	if(IsPathPartOfAssetFolder(pathToAsset))
	{
		std::string relativePath;

		// Normalize path and get iterator
		fs::path normalizedPath = fs::canonical(pathToAsset);
		fs::path::iterator pathIterator = normalizedPath.begin();

		// Normalize path to asset folder
		pathToAssetFolder = fs::canonical(pathToAssetFolder);

		// Iterate over path to asset until asset folder has been reached.
		for (fs::path::iterator i = pathToAssetFolder.begin(); i != pathToAssetFolder.end(); ++i) {
			++pathIterator;
		}

		// Add remaining path to relativePath string
		for (pathIterator; pathIterator != normalizedPath.end(); ++pathIterator)
		{
			relativePath.append(pathIterator->string()).append("/");
		}

		// Remove excess / and file extension
		if(relativePath.size() > 0)
		{
			// Remove /
			relativePath.resize(relativePath.size() - 1);

			// Remove extension
			int lastIndex = relativePath.find_last_of(".");
			relativePath.resize(lastIndex);
		}

		return relativePath;
	} 

	return pathToAsset.string();
}


std::string FileManager::GetRelativeAssetPathFromFullPath(std::string pathToAsset)
{
	return GetRelativeAssetPathFromFullPath(fs::path{pathToAsset});
}


bool FileManager::IsPathPartOfAssetFolder(boost::filesystem::path pathToAsset)
{
	// Iterate over path to asset folder and compare it to the given path.
	// If it differs, return false.

	// Normalize given path and get iterator
	fs::path normalizedPath = fs::canonical(pathToAsset);
	fs::path::iterator givenPathIterator = normalizedPath.begin();

	// Get path to asset folder and normalize it
	fs::path pathToAssetFolder(GetAssetDirectory());
	pathToAssetFolder = fs::canonical(pathToAssetFolder);

	for (fs::path::iterator i = pathToAssetFolder.begin(); i != pathToAssetFolder.end(); ++i)
	{
		if (*i != *givenPathIterator) 
		{
			//printf("Difference found.\n");
			return false;
		}
		++givenPathIterator;
	}

	return true;
}


std::vector<std::string> FileManager::GetAllFilesWithExtension(std::string pathToDirectory, std::string fileExtension)
{
	std::vector<std::string> output;
	fs::path path(pathToDirectory);

	// Check if the directory exists and is a directory in the first place
	// If not, return the empty vector
	if (!fs::exists(path) || !fs::is_directory(path))
		return output;

	fs::recursive_directory_iterator fileIterator(path);
	fs::recursive_directory_iterator endingIterator;

	// Iterate through the files
	while(fileIterator != endingIterator)
	{
		// If the current iterator points to a file and the extension matches ...
		if(fs::is_regular_file(*fileIterator) && fileIterator->path().extension() == fileExtension)
		{
			// Add it to the vector!
			output.push_back(fileIterator->path().string());
		}

		++fileIterator;
	}

	return output;
}

std::vector<std::string> FileManager::GetAllFilesWithExtensionWithSubdirectories(std::string pathToDirectory, std::string fileExtension)
{
	std::vector<std::string> output;
	fs::path path(pathToDirectory);

	// Check if the directory exists and is a directory in the first place
	// If not, return the empty vector
	if (!fs::exists(path) || !fs::is_directory(path))
		return output;

	fs::recursive_directory_iterator fileIterator(path);
	fs::recursive_directory_iterator endingIterator;

	// Iterate through the files
	while (fileIterator != endingIterator) 
	{
		// If the current iterator points to a file and the extension matches ...
		if (fs::is_regular_file(*fileIterator) && fileIterator->path().extension() == fileExtension) 
		{
			// Check the meta file if the file should be skipped
			if(MetaFileExists(*fileIterator, true))
			{
				// Check whether to load the file or not.
				if(CheckIgnorePreload(GetPathToMetaFile(*fileIterator)) == false)
				{
					output.push_back(fileIterator->path().string());
				}
				else
				{
					// Ignoring file
					//printf("Ignoring %s due to meta file...\n", fileIterator->path().filename().string().c_str());
				}
			}
			else
			{
				// No meta file found?
				// Load it in!
				output.push_back(fileIterator->path().string());
			}
		} 
		else if(fs::is_directory(*fileIterator))
		{
			// Hey, look! Recursion! Cool!
			// Hey, look! Recursion! Cool!
			// Hey, look! Recursion! Cool!
			// Hey, look! Recursion! Cool!
			// Hey, look! Recursion! Cool!
			// Hey, look! Recursion! Cool!
			// Hey, look! Recursion! Cool!
			// No, really though. If a subfolder is found: Delve into it and return the goods!
			std::vector<std::string> subFolderFiles = GetAllFilesWithExtensionWithSubdirectories(fileIterator->path().string(), fileExtension);
			
			// Okay, look at this line right here:
			// output.insert(output.end(), subFolderFiles.begin(), subFolderFiles.end());

			// Now, I know this can end up multiplying my results and I could account for that.
			// But WHY on earth does this work without appending the subfolders to the output?
			// It still works this way! HOW?!
		}

		++fileIterator;
	}

	return output;
}


fs::path GetPathToMetaFile(fs::path originalPath)
{
	fs::path metaPath = originalPath.parent_path();
	std::string metaFileName = ".";
	metaFileName.append(originalPath.stem().string());
	metaFileName.append(".xml");
	metaPath.append(metaFileName);

	return metaPath;
}

bool MetaFileExists(fs::path originalFilePath, bool createMissingFile)
{
	// Directories (folders) don't need meta files in my engine. Just nod, smile, say yes and move on.
	if (!fs::exists(originalFilePath) || fs::is_directory(originalFilePath))
		return true;

	// Get meta file name
	fs::path metaPath = GetPathToMetaFile(originalFilePath);

	if(fs::exists(metaPath))
		return true;

	if (!createMissingFile)
		return false;

	return CreateMetaFile(metaPath);
}

bool CreateMetaFile(fs::path filePath)
{
	tx::XMLDocument newMetaDoc;
	
	// Look at this:
	// http://www.grinninglizard.com/tinyxml2docs/classtinyxml2_1_1_x_m_l_declaration.html
	// TinyXML2 actually does not need a declaration; neat!

	// Let's create the root node, then.
	tx::XMLNode *rootNode = newMetaDoc.NewElement("MetaFileInfo");
	newMetaDoc.InsertFirstChild(rootNode);

	// IgnorePreload tells the FileManager not to add the file to the respective buffer.
	// This can be used for large files like music and backgrounds in order to save on memory.
	tx::XMLElement *ignorePreloadNode = newMetaDoc.NewElement("IgnorePreload");
	ignorePreloadNode->SetText(0);

	rootNode->InsertEndChild(ignorePreloadNode);

	tx::XMLError error = newMetaDoc.SaveFile(filePath.string().c_str());
	if (error == tx::XML_NO_ERROR)
		return true;

	return false;
}


bool CheckIgnorePreload(fs::path metaFilePath)
{
	tx::XMLDocument metaFile;
	metaFile.LoadFile(metaFilePath.string().c_str());

	tx::XMLElement *ignorePreloadNode = metaFile.FirstChildElement("MetaFileInfo")->FirstChildElement("IgnorePreload");
	if (ignorePreloadNode != NULL) 
	{
		// Check if content of node is a number or not
		// If it's not a number, treat it as "false"
		int ignorePreloadContent;
		try 
		{
			ignorePreloadContent = boost::lexical_cast<int>(ignorePreloadNode->GetText());
			if (ignorePreloadContent == 1)
				return true;
			return false;
		} 
		catch (const boost::bad_lexical_cast)
		{
			// It's not a number.
			return false;
		}
	}

	return false;
}
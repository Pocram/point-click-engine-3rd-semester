#ifndef __InventoryH
#define __InventoryH

#include "Item.h"

namespace Inventory
{
	// Methods
	void InitInventory();

	Item* GetItemInSlot(int slotId);

	void UnselectItem();
	Item* SelectItemInSlot(int slotId);
	Item* GetCurrentlySelectedItem();
	
	Item* GetCurrentlyHoveredItem();
	void HoverItem(int slotId);
	void UnHoverItem();

	int GetAmountOfItems();

	void AddItemToInventory(int itemId);
	void AddItemToInventory(Item item);

	void RemoveItemFromInventory(int itemId);
}

#endif // __InventoryH
#include "Item.h"

Item::Item()
{
	m_ID = -1;
	m_Name = "Invalid Item";
	m_IsValid = false;
}

Item::~Item() {}


bool Item::UseWithObject(GameObject* gameObject)
{
	return true;
}

bool Item::UseWithItem(Item* item)
{
	return true;
}

void Item::RemoveFromInventory()
{
}

std::string Item::GetLookAtText()
{
	return m_lookAtText;
}

void Item::SetLookAtText(std::string text)
{
	m_lookAtText = text;
}

void Item::SetTexture(sf::Texture *texture)
{
	m_texture = texture;
}

sf::Texture* Item::GetTexture()
{
	return m_texture;
}
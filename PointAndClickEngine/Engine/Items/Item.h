#ifndef __ItemH
#define __ItemH

#include "SFML/Graphics.hpp"

#include <Engine/GameObjects/GameObject.h>


class Item {

public:
	// Constructors
	Item();
	~Item();


	// Members
	int m_ID;
	std::string m_Name;
	bool m_IsValid;


	// Methods
	bool UseWithObject(GameObject* gameObject);
	bool UseWithItem(Item* item);

	void RemoveFromInventory();

	std::string GetLookAtText();
	void SetLookAtText(std::string text);

	void SetTexture(sf::Texture *texture);
	sf::Texture* GetTexture();

private:
	// Members
	sf::Texture *m_texture;
	std::string m_lookAtText;

	// Methods

};

#endif // __ITemH
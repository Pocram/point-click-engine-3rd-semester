#include "Inventory.h"
#include <Engine/File Manager/ItemManager.h>
#include <SFML/Audio.hpp>
#include <Engine/File Manager/SoundManager.h>

std::vector<Item> _Inventory;
Item* _CurrentlySelectedItem;
Item* _CurrentlyHoveredItem;

sf::Sound _PickUpAudio;


void Inventory::InitInventory()
{
	_Inventory = std::vector<Item>();
	_CurrentlySelectedItem = NULL;

	sf::SoundBuffer *sound = SoundManager::GetSoundBuffer("Audio/SFX/fabric");
	if(sound != NULL)
	{
		_PickUpAudio.setBuffer(*sound);
	}
}

Item* Inventory::GetItemInSlot(int slotId)
{
	if (slotId < 0)
		return NULL;

	int size = _Inventory.size() - 1;
	// Okay, here me out. Yes, I have to save this number in a separate integer. "But why?" you ask. Good question!
	// Apparently 'size()' returns a value of type 'size'. While size can be used as an integer, it produces absolutely different results when used in different contexts:
	// int size = _Inventory.size() - 1; If size() is 0, this returns -1. Sure thing!
	// if(slotId > _Inventory.size() - 1) ... Here '_Inventory.size() - 1' returns 0. WHY? JUST WHY?

	if (slotId > size)
	{
		return NULL;
	}

	return &_Inventory[slotId];
}

void Inventory::UnselectItem()
{
	printf("Unselecting item.\n");
	_CurrentlySelectedItem = NULL;
}

Item* Inventory::SelectItemInSlot(int slotId)
{
	if (slotId < 0)
		return NULL;

	int size = _Inventory.size() - 1;
	if (slotId > size)
		return NULL;

	printf("Selecting item '%s' in slot %d.\n", _Inventory[slotId].m_Name.c_str(), slotId);
	_CurrentlySelectedItem = &_Inventory[slotId];
	return &_Inventory[slotId];
}

Item* Inventory::GetCurrentlySelectedItem()
{
	return _CurrentlySelectedItem;
}

Item* Inventory::GetCurrentlyHoveredItem()
{
	return _CurrentlyHoveredItem;
}

void Inventory::HoverItem(int slotId)
{
	if (slotId < 0)
		return;

	int size = _Inventory.size() - 1;
	if (slotId > size)
		return;

	_CurrentlyHoveredItem = &_Inventory[slotId];
}

void Inventory::UnHoverItem()
{
	_CurrentlyHoveredItem = NULL;
}

int Inventory::GetAmountOfItems()
{
	return _Inventory.size();
}

void Inventory::AddItemToInventory(int itemId)
{
	// Look for the item in the manager and add it
	Item* item = ItemManager::GetItem(itemId);

	if(item == NULL)
	{
		printf("- ERROR: Item with id '%d' could not be found!\n", itemId);
		return;
	}

	AddItemToInventory(*item);
}

void Inventory::AddItemToInventory(Item item)
{
	// Only add real and valid items
	if (!item.m_IsValid)
		return;

	if (_Inventory.size() > 10)
	{
		printf("The item %d: '%s' was added to the inventory but the inventory was already full and was thus discarded.\n", item.m_ID, item.m_Name.c_str());
		return;
	}

	_Inventory.push_back(item);
	_PickUpAudio.play();
	printf("The item %d: '%s' was added to the inventory.\n", item.m_ID, item.m_Name.c_str());
}

void Inventory::RemoveItemFromInventory(int itemId)
{
	// Iterate over every item in the inventory
	std::vector<Item>::iterator i = _Inventory.begin();
	for (i; i != _Inventory.end(); ++i)
	{
		// If the IDs match, remove it and stop
		if (i->m_ID == itemId) {
			_Inventory.erase(i);
			return;
		}
	}
}
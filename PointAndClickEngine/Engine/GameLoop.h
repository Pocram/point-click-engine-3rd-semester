#ifndef __GameLoop
#define __GameLoop

#include <SFML/Graphics.hpp>
#include <Engine/Scenes/GameScene.h>

void StartGameLoop(sf::RenderWindow * window);

// Loop elements
// Tells the current scene to update
void Update(sf::Time elapsedTime);

// Tells the current scene to render
void Draw(sf::RenderWindow *window);


// Control elements
// Unloads the current scene and loads the new one.
void LoadScene(GameScene scene);

void _LoadSceneInternal();

#endif // __GameLoop